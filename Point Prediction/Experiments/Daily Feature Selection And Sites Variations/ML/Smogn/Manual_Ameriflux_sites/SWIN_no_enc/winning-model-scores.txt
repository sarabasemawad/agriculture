
Utility Based Metrics
F1: 0.73898
F2: 0.71505
F05: 0.76455
precision: 0.78261
recall: 0.69995

Regression Error Metrics
R2: 0.41775
Adj-R2: 0.40748
RMSE: 1.45015
MSE: 2.10293
MAE: 1.10404
MAPE: 41.28125

Correlations
Pearson: 0.66713
Spearman: 0.69650
Distance: 0.33287

Utility Based Metrics
F1: 0.71182
F2: 0.70124
F05: 0.72273
precision: 0.73018
recall: 0.69436

Regression Error Metrics
R2: 0.40563
Adj-R2: 0.39515
RMSE: 1.46517
MSE: 2.14672
MAE: 1.11308
MAPE: 41.56429

Correlations
Pearson: 0.65882
Spearman: 0.69253
Distance: 0.34118

Utility Based Metrics
F1: 0.73067
F2: 0.72584
F05: 0.73557
precision: 0.73887
recall: 0.72265

Regression Error Metrics
R2: 0.50645
Adj-R2: 0.49469
RMSE: 1.33514
MSE: 1.78259
MAE: 0.99016
MAPE: 35.98769

Correlations
Pearson: 0.72726
Spearman: 0.75770
Distance: 0.27274

Utility Based Metrics
F1: 0.78094
F2: 0.78256
F05: 0.77932
precision: 0.77825
recall: 0.78365

Regression Error Metrics
R2: 0.54476
Adj-R2: 0.53651
RMSE: 1.28227
MSE: 1.64423
MAE: 0.97926
MAPE: 36.26058

Correlations
Pearson: 0.75876
Spearman: 0.76352
Distance: 0.24124

Utility Based Metrics
F1: 0.79766
F2: 0.80074
F05: 0.79461
precision: 0.79259
recall: 0.80280

Regression Error Metrics
R2: 0.60224
Adj-R2: 0.59257
RMSE: 1.19859
MSE: 1.43663
MAE: 0.90840
MAPE: 33.70322

Correlations
Pearson: 0.79019
Spearman: 0.79028
Distance: 0.20981

Utility Based Metrics
F1: 0.71139
F2: 0.70351
F05: 0.71945
precision: 0.72493
recall: 0.69835

Regression Error Metrics
R2: 0.42400
Adj-R2: 0.41196
RMSE: 1.44215
MSE: 2.07979
MAE: 1.07562
MAPE: 39.57801

Correlations
Pearson: 0.67033
Spearman: 0.71136
Distance: 0.32967

Utility Based Metrics
F1: 0.79681
F2: 0.80069
F05: 0.79296
precision: 0.79041
recall: 0.80330

Regression Error Metrics
R2: 0.57754
Adj-R2: 0.56847
RMSE: 1.23507
MSE: 1.52540
MAE: 0.94816
MAPE: 34.30630

Correlations
Pearson: 0.78162
Spearman: 0.78674
Distance: 0.21838
