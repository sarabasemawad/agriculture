
Utility Based Metrics
F1: 0.68014
F2: 0.71998
F05: 0.64448
precision: 0.62272
recall: 0.74924

Regression Error Metrics
R2: 0.44071
Adj-R2: 0.41394
RMSE: 1.77120
MSE: 3.13715
MAE: 1.33602
MAPE: 42.91683

Correlations
Pearson: 0.66984
Spearman: 0.69852
Distance: 0.33016
