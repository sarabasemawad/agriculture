
Utility Based Metrics
F1: 0.67570
F2: 0.66914
F05: 0.68239
precision: 0.68692
recall: 0.66484

Regression Error Metrics
R2: 0.33258
Adj-R2: 0.24690
RMSE: 1.55328
MSE: 2.41269
MAE: 1.17495
MAPE: 44.44962

Correlations
Pearson: 0.59387
Spearman: 0.61382
Distance: 0.40613

Utility Based Metrics
F1: 0.67959
F2: 0.68123
F05: 0.67796
precision: 0.67687
recall: 0.68232

Regression Error Metrics
R2: 0.36334
Adj-R2: 0.28160
RMSE: 1.51707
MSE: 2.30151
MAE: 1.18690
MAPE: 43.95162

Correlations
Pearson: 0.62139
Spearman: 0.64286
Distance: 0.37861

Utility Based Metrics
F1: 0.69974
F2: 0.73634
F05: 0.66660
precision: 0.64620
recall: 0.76295

Regression Error Metrics
R2: 0.33599
Adj-R2: 0.23524
RMSE: 1.54931
MSE: 2.40037
MAE: 1.09509
MAPE: 42.76022

Correlations
Pearson: 0.62795
Spearman: 0.63989
Distance: 0.37205

Utility Based Metrics
F1: 0.66479
F2: 0.72437
F05: 0.61426
precision: 0.58464
recall: 0.77040

Regression Error Metrics
R2: 0.28620
Adj-R2: 0.17790
RMSE: 1.60635
MSE: 2.58036
MAE: 1.13112
MAPE: 44.15257

Correlations
Pearson: 0.61244
Spearman: 0.64114
Distance: 0.38756

Utility Based Metrics
F1: 0.70729
F2: 0.70981
F05: 0.70479
precision: 0.70313
recall: 0.71150

Regression Error Metrics
R2: 0.35651
Adj-R2: 0.24055
RMSE: 1.52518
MSE: 2.32618
MAE: 1.17763
MAPE: 43.11148

Correlations
Pearson: 0.62842
Spearman: 0.62821
Distance: 0.37158
