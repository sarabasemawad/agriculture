
Utility Based Metrics
F1: 0.75436
F2: 0.70932
F05: 0.80551
precision: 0.84365
recall: 0.68217

Regression Error Metrics
R2: 0.23091
Adj-R2: 0.00471
RMSE: 1.11019
MSE: 1.23252
MAE: 0.80744
MAPE: 33.56144

Correlations
Pearson: 0.51832
Spearman: 0.48341
Distance: 0.48168

Utility Based Metrics
F1: 0.75142
F2: 0.70649
F05: 0.80244
precision: 0.84049
recall: 0.67942

Regression Error Metrics
R2: 0.23189
Adj-R2: 0.00598
RMSE: 1.10948
MSE: 1.23095
MAE: 0.81859
MAPE: 34.06000

Correlations
Pearson: 0.51883
Spearman: 0.49839
Distance: 0.48117

Utility Based Metrics
F1: 0.74085
F2: 0.70311
F05: 0.78286
precision: 0.81363
recall: 0.68002

Regression Error Metrics
R2: 0.24643
Adj-R2: -0.01089
RMSE: 1.09893
MSE: 1.20765
MAE: 0.77301
MAPE: 32.03315

Correlations
Pearson: 0.52297
Spearman: 0.51185
Distance: 0.47703
