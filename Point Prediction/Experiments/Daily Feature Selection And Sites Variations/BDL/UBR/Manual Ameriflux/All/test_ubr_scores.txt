Utility Based Metrics
F1: 0.87020
F2: 0.86038
F05: 0.88025
precision: 0.88707
recall: 0.85395

Regression Error Metrics
R2: 0.72
Adj-R2: 0.71
RMSE: 1.05
MSE: 1.10
MAE: 0.76
MAPE: 27.28
Accuracy: 72.72
AIC: 24362.72
BIC: 91167.19
Normalized Mutal Information: 1

Correlations
Pearson: 0.85
Spearman: 0.84
Distance: 0.15