Utility Based Metrics
F1: 0.00001
F2: 0.00001
F05: 0.00001
precision: 0.00001
recall: 0.00001

Regression Error Metrics
Average of test output: 3.2044580616369465
Average of output: 3.416304752104188
R2: 0.51
Adj-R2: 0.41
RMSE: 1.24
MSE: 1.54
MAE: 0.91
MAPE: 37.15
Accuracy: 62.85
AIC: 23553.11
BIC: 66698.88
Normalized Mutal Information: 1

Correlations
Pearson: 0.73
Spearman: 0.76
Distance: 0.27
Data Size: 1471
Training Start time: 1593289332.1535668
Training time in seconds: 151.48484 seconds
Testing Start time: 1593289483.7520409
Testing time in seconds: 0.00004 seconds
