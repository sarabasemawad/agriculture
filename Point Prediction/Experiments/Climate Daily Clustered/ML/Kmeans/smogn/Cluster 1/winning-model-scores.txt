
Utility Based Metrics
F1: 0.79035
F2: 0.79682
F05: 0.78400
precision: 0.77982
recall: 0.80118

Regression Error Metrics
R2: 0.48861
Adj-R2: 0.45767
RMSE: 1.15633
MSE: 1.33709
MAE: 0.91844
MAPE: 37.62822

Correlations
Pearson: 0.75263
Spearman: 0.74397
Distance: 0.24737

Utility Based Metrics
F1: 0.85018
F2: 0.87129
F05: 0.83007
precision: 0.81718
recall: 0.88596

Regression Error Metrics
R2: 0.63206
Adj-R2: 0.56409
RMSE: 0.94873
MSE: 0.90009
MAE: 0.75540
MAPE: 34.44332

Correlations
Pearson: 0.85066
Spearman: 0.82195
Distance: 0.14934
