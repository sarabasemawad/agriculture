
Utility Based Metrics
F1: 0.85657
F2: 0.87924
F05: 0.83504
precision: 0.82127
recall: 0.89504

Regression Error Metrics
R2: 0.59862
Adj-R2: 0.52447
RMSE: 0.99091
MSE: 0.98190
MAE: 0.77235
MAPE: 35.16819

Correlations
Pearson: 0.83859
Spearman: 0.82017
Distance: 0.16141
