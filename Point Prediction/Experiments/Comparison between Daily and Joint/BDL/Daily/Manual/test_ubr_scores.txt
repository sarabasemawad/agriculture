Utility Based Metrics
F1: 0.83099
F2: 0.81749
F05: 0.84494
precision: 0.85450
recall: 0.80873

Regression Error Metrics
R2: 0.63
Adj-R2: 0.62
RMSE: 1.21
MSE: 1.47
MAE: 0.90
MAPE: 30.50
Accuracy: 69.50
AIC: 24268.89
BIC: 90892.4
Normalized Mutal Information: 1

Correlations
Pearson: 0.79
Spearman: 0.78
Distance: 0.21