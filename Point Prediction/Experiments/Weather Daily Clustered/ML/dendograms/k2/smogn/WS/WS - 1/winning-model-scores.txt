
Utility Based Metrics
F1: 0.69782
F2: 0.68261
F05: 0.71373
precision: 0.72474
recall: 0.67283

Regression Error Metrics
R2: 0.27291
Adj-R2: 0.19194
RMSE: 1.52690
MSE: 2.33143
MAE: 1.10330
MAPE: 40.94329

Correlations
Pearson: 0.56759
Spearman: 0.61847
Distance: 0.43241
