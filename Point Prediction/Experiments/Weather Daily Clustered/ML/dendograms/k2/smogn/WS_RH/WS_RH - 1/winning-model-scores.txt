
Utility Based Metrics
F1: 0.00002
F2: 0.00005
F05: 0.00001
precision: 0.00001
recall: 0.72325

Regression Error Metrics
R2: 0.54905
Adj-R2: 0.52711
RMSE: 1.22699
MSE: 1.50551
MAE: 0.90294
MAPE: 32.95635

Correlations
Pearson: 0.75136
Spearman: 0.77106
Distance: 0.24864
