
Utility Based Metrics
F1: 0.78680
F2: 0.75723
F05: 0.81877
precision: 0.84157
recall: 0.73872

Regression Error Metrics
R2: 0.56033
Adj-R2: 0.53850
RMSE: 1.40372
MSE: 1.97042
MAE: 1.04303
MAPE: 37.25290

Correlations
Pearson: 0.75601
Spearman: 0.78696
Distance: 0.24399
