
Utility Based Metrics
F1: 0.75281
F2: 0.74173
F05: 0.76423
precision: 0.77204
recall: 0.73452

Regression Error Metrics
R2: 0.47104
Adj-R2: 0.45458
RMSE: 1.39070
MSE: 1.93406
MAE: 1.05971
MAPE: 37.18975

Correlations
Pearson: 0.70979
Spearman: 0.73542
Distance: 0.29021
