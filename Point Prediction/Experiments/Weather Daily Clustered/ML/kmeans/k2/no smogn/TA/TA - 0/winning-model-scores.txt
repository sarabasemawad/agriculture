
Utility Based Metrics
F1: 0.74921
F2: 0.72992
F05: 0.76954
precision: 0.78372
recall: 0.71761

Regression Error Metrics
R2: 0.46153
Adj-R2: 0.44483
RMSE: 1.37855
MSE: 1.90041
MAE: 1.05192
MAPE: 37.16086

Correlations
Pearson: 0.70273
Spearman: 0.73605
Distance: 0.29727
