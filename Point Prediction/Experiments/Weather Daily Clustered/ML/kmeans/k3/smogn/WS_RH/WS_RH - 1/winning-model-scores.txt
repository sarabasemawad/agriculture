
Utility Based Metrics
F1: 0.72488
F2: 0.71298
F05: 0.73719
precision: 0.74563
recall: 0.70526

Regression Error Metrics
R2: 0.31012
Adj-R2: 0.24512
RMSE: 1.47522
MSE: 2.17627
MAE: 1.09993
MAPE: 39.82953

Correlations
Pearson: 0.58768
Spearman: 0.62923
Distance: 0.41232
