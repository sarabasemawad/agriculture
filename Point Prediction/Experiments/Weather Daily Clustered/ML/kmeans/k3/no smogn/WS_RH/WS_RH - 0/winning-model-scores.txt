
Utility Based Metrics
F1: 0.80054
F2: 0.76447
F05: 0.84018
precision: 0.86886
recall: 0.74217

Regression Error Metrics
R2: 0.54777
Adj-R2: 0.51414
RMSE: 1.45878
MSE: 2.12804
MAE: 1.07524
MAPE: 39.51464

Correlations
Pearson: 0.74611
Spearman: 0.75914
Distance: 0.25389
