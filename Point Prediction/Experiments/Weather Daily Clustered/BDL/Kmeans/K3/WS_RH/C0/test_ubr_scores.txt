Utility Based Metrics
F1: 0.72855
F2: 0.75063
F05: 0.70774
precision: 0.69451
recall: 0.76611

Regression Error Metrics
Average of test output: 3.87401237753845
Average of output: 3.831490205506257
R2: 0.52
Adj-R2: 0.49
RMSE: 1.53
MSE: 2.36
MAE: 1.06
MAPE: 35.04
Accuracy: 64.96
AIC: 23897.84
BIC: 76913.37
Normalized Mutal Information: 1

Correlations
Pearson: 0.73
Spearman: 0.75
Distance: 0.27
Data Size: 3495
Training Start time: 1593203285.1627998
Training time in seconds: 102.91942 seconds
Testing Start time: 1593203388.182097
Testing time in seconds: 0.00002 seconds