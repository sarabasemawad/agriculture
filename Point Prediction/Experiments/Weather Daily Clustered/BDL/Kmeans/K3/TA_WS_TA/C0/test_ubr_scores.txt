Utility Based Metrics
F1: 0.00002
F2: 0.00005
F05: 0.00001
precision: 0.00001
recall: 0.74335

Regression Error Metrics
Average of test output: 4.248982857140616
Average of output: 4.190281449541959
R2: 0.60
Adj-R2: 0.58
RMSE: 1.20
MSE: 1.45
MAE: 0.92
MAPE: 28.29
Accuracy: 71.71
AIC: 23614.51
BIC: 78905.46
Normalized Mutal Information: 1

Correlations
Pearson: 0.78
Spearman: 0.79
Distance: 0.22
Data Size: 4253
Training Start time: 1593213070.3513093
Training time in seconds: 109.94849 seconds
Testing Start time: 1593213180.4023447
Testing time in seconds: 0.00002 seconds
