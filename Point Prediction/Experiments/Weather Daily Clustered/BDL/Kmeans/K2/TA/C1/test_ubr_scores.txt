Utility Based Metrics
F1: 0.79573
F2: 0.78393
F05: 0.80790
precision: 0.81622
recall: 0.77625

Regression Error Metrics
Average of test output: 2.384264110565475
Average of output: 2.393414356116259
R2: 0.49
Adj-R2: 0.44
RMSE: 0.88
MSE: 0.77
MAE: 0.67
MAPE: 32.95
Accuracy: 67.05
AIC: 23279.21
BIC: 73993.23
Normalized Mutal Information: 1

Correlations
Pearson: 0.70
Spearman: 0.61
Distance: 0.30