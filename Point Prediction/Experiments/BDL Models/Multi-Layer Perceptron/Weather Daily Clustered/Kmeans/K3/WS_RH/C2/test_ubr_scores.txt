Utility Based Metrics
F1: 0.82764
F2: 0.79971
F05: 0.85759
precision: 0.87879
recall: 0.78212

Regression Error Metrics
Average of test output: 3.603421803701804
Average of output: 3.6899371020037175
R2: 0.64
Adj-R2: 0.62
RMSE: 1.09
MSE: 1.19
MAE: 0.81
MAPE: 28.33
Accuracy: 71.67
AIC: 23456.48
BIC: 79680.91
Normalized Mutal Information: 1

Correlations
Pearson: 0.81
Spearman: 0.80
Distance: 0.19
Data Size: 4611

Training Start time: 1593203944.3206623
Training time in seconds: 156.27156 seconds
Testing Start time: 1593204100.6946194
Testing time in seconds: 0.00001 seconds