Utility Based Metrics
F1: 0.81581
F2: 0.77531
F05: 0.86078
precision: 0.89361
recall: 0.75048

Regression Error Metrics
Average of test output: 2.454253826324637
Average of output: 2.4700761449407413
R2: 0.51
Adj-R2: 0.46
RMSE: 0.86
MSE: 0.75
MAE: 0.66
MAPE: 31.57
Accuracy: 68.43
AIC: 23261.0
BIC: 74037.49
Normalized Mutal Information: 1

Correlations
Pearson: 0.71
Spearman: 0.65
Distance: 0.29