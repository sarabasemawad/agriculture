Utility Based Metrics
F1: 0.84806
F2: 0.83044
F05: 0.86644
precision: 0.87914
recall: 0.81910

Regression Error Metrics
Average of test output: 3.4884078308488324
Average of output: 3.6842810933102923
R2: 0.60
Adj-R2: 0.58
RMSE: 1.14
MSE: 1.29
MAE: 0.83
MAPE: 31.45
Accuracy: 68.55
AIC: 23739.04
BIC: 83610.18
Normalized Mutal Information: 1

Correlations
Pearson: 0.78
Spearman: 0.77
Distance: 0.22