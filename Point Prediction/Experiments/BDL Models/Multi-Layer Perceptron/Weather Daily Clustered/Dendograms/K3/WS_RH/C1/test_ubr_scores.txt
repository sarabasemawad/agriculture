Utility Based Metrics
F1: 0.80443
F2: 0.83782
F05: 0.77360
precision: 0.75433
recall: 0.86167

Regression Error Metrics
Average of test output: 3.5645781088982655
Average of output: 3.781547833544993
R2: 0.49
Adj-R2: 0.45
RMSE: 1.41
MSE: 1.98
MAE: 1.04
MAPE: 38.82
Accuracy: 61.18
AIC: 23816.52
BIC: 77790.05
Normalized Mutal Information: 1

Correlations
Pearson: 0.74
Spearman: 0.73
Distance: 0.26
Data Size: 3808
Training Start time: 1593205257.9799607
Training time in seconds: 109.48105 seconds
Testing Start time: 1593205367.558398
Testing time in seconds: 0.00001 seconds