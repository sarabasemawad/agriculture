Utility Based Metrics
F1: 0.76979
F2: 0.77872
F05: 0.76106
precision: 0.75534
recall: 0.78479

Regression Error Metrics
Average of test output: 4.6123520281228245
Average of output: 4.865892217266243
R2: 0.55
Adj-R2: 0.52
RMSE: 1.42
MSE: 2.03
MAE: 1.06
MAPE: 33.52
Accuracy: 66.48
AIC: 23857.36
BIC: 78281.87
Normalized Mutal Information: 1

Correlations
Pearson: 0.75
Spearman: 0.75
Distance: 0.25
Data Size: 3959
Training Start time: 1593211225.434649
Training time in seconds: 135.59216 seconds
Testing Start time: 1593211361.1303787
Testing time in seconds: 0.00002 seconds
