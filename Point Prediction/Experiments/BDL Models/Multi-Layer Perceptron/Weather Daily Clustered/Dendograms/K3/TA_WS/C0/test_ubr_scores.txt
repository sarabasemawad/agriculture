Utility Based Metrics
F1: 0.78000
F2: 0.76590
F05: 0.79463
precision: 0.80469
recall: 0.75678

Regression Error Metrics
Average of test output: 2.5460485239360677
Average of output: 2.5284754144296806
R2: 0.49
Adj-R2: 0.45
RMSE: 0.92
MSE: 0.84
MAE: 0.68
MAPE: 30.90
Accuracy: 69.10
AIC: 23192.46
BIC: 74662.0
Normalized Mutal Information: 1

Correlations
Pearson: 0.71
Spearman: 0.64
Distance: 0.29
Data Size: 3066
Training Start time: 1593208974.5009882
Training time in seconds: 95.81060 seconds
Testing Start time: 1593209070.4089634
Testing time in seconds: 0.00001 seconds