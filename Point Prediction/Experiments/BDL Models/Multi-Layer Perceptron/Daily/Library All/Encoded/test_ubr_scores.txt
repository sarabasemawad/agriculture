Utility Based Metrics
F1: 0.82663
F2: 0.81069
F05: 0.84321
precision: 0.85464
recall: 0.80041

Regression Error Metrics
R2: 0.60
Adj-R2: 0.58
RMSE: 1.42
MSE: 2.02
MAE: 1.00
MAPE: 33.71
Accuracy: 66.29
AIC: 24147.49
BIC: 81909.82
Normalized Mutal Information: 1

Correlations
Pearson: 0.77
Spearman: 0.76
Distance: 0.23