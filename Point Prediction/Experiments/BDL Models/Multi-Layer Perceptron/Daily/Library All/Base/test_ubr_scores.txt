Utility Based Metrics
F1: 0.72050
F2: 0.73724
F05: 0.70450
precision: 0.69422
recall: 0.74884

Regression Error Metrics
R2: 0.36
Adj-R2: 0.34
RMSE: 1.79
MSE: 3.21
MAE: 1.27
MAPE: 44.60
Accuracy: 55.40
AIC: 22956.77
BIC: 76616.12
Normalized Mutal Information: 1

Correlations
Pearson: 0.64
Spearman: 0.65
Distance: 0.36