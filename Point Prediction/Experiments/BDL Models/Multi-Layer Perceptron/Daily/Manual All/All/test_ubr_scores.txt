F1: 0.87755
F2: 0.87048
F05: 0.88475
precision: 0.88961
recall: 0.86582

Regression Error Metrics
R2: 0.70
Adj-R2: 0.69
RMSE: 1.09
MSE: 1.18
MAE: 0.78
MAPE: 27.15
Accuracy: 72.85
AIC: 24562.82
BIC: 93370.52
Normalized Mutal Information: 1

Correlations
Pearson: 0.84
Spearman: 0.83
Distance: 0.16