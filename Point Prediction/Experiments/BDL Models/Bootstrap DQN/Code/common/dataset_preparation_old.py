import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
import datetime
import json
import time
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
import category_encoders as ce


from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.metrics.cluster import normalized_mutual_info_score
from scipy.stats.stats import pearsonr, spearmanr

import warnings
warnings.filterwarnings("ignore")

from collections import OrderedDict

output_column = "LE_bowen_corr(mm)"
output_path = "output/"

class DataPreparation():
    def __init__(self):
         self.read_data()

    def read_data(self):
        input_path = "All_Manual_Daily_Albedo_NDVI_LST_Cleaned.csv"
        df = pd.read_csv(input_path, delimiter=',')

        columnsToDrop = ['Date', 'Year', 'Month', 'Day', 'Site Id_1', 'Site Id_2', 'Site Id_3',
                        'Site Id_4', 'Site Id_5', 'Site Id_6','Vegetation', 'Latitude', 'Longitude', 'Climate',
                        'G','G-1','G-2','G-3','G-4','G-5',
                        'Climate_1', 'Climate_2', 'Climate_3',
                        'Latitude_1','Latitude_2', 'Latitude_3', 'Latitude_4', 'Latitude_5',
                        'Latitude_6','Longitude_1', 'Longitude_2', 'Longitude_3', 'Longitude_4',
                        'Longitude_5', 'Longitude_6',
                        'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
                        'H_bowen_corr-5', 'C_BOWENS',
                        'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
                        'LE', 'LE_bowen_corr',
                        'Elevation(m)_1','Elevation(m)_2', 'Elevation(m)_3', 'Elevation(m)_4',
                        'Elevation(m)_5', 'Elevation(m)_6',
                        'ETo', 'EToF', 'ETr', 'ETrF', 'ETo', 'SW_IN']

        df = df.drop(columnsToDrop, axis = 1)
        df.dropna(inplace=True)

        df = df[df[output_column].between(1, 15)]

        #Generate 5 lags
        lagsForColumns = ["SW_IN", "WS", "RH", "TA", "EEflux LST", "EEflux Albedo", "EEflux NDVI"]
        df = self.generate_lags(df, lagsForColumns)
        df.dropna(inplace=True)

        #Binary Encode Site Id
        df = self.binary_encode_column(df, "Site Id")
        df.drop(columns=['Site Id_0'], inplace=True)

        #Use for all other evaluations
        X_train, Y_train, X_test, Y_test = self.split_train_test(df, 0.8)
         #Use for combined evaluation
        # X_train, Y_train, X_test, Y_test = self.split_train_test_default(df, 4000, 4000)
           
        print("X_train shape:", X_train.shape, "X_test shape:", X_test.shape)
        print("Y_train shape:", Y_train.shape, "Y_test shape:", Y_test.shape)
        columnToDrop = ["Site Id"]
        X_train.drop(columnToDrop, axis = 1, inplace=True)
        X_test.drop(columnToDrop, axis = 1, inplace=True)

        scaler = MinMaxScaler()
        X_train_scaled = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)
        return X_train_scaled, Y_train, X_test_scaled, Y_test

    def binary_encode_column(self, df, columnToEncode):
        encoder = ce.BinaryEncoder(cols=[columnToEncode])
        df_encoder = encoder.fit_transform(df[columnToEncode])
        df = pd.concat([df, df_encoder], axis=1)
        return df

    def generate_lags_for(self, df, column, lags_count): 
            for i in range(lags_count):
                lag_name = column + "-" + str(i + 1)
                df[lag_name] = df[column].shift(i + 1)
            return df

    def generate_lags(self, df, lagsForColumns):
        for k in range(len(lagsForColumns)):
            col = lagsForColumns[k]
            if col in df.columns:
                df = self.generate_lags_for(df, col, 5)
        return df

    def split_data_sample(self, df, k=1000):
        df_sub = df.sample(n=k, replace=False)
        X = df_sub.drop([output_column], axis = 1)
        Y = df_sub[output_column]
        Y = np.array(Y).reshape(-1, 1)
        return X, Y

    def split_train_test_default(self, df, rows, test_rows):
        df_train = df.iloc[:rows, ]
        df_test = df.iloc[rows:rows+test_rows, ]
        print("df train shape:", df_train.shape, "df test shape:", df_test.shape)
        X_train, Y_train = self.split_features_label(df_train)
        X_test, Y_test = self.split_features_label(df_test)
        return X_train, Y_train, X_test, Y_test

    def split_features_label(self, df):
        X = df.drop([output_column], axis = 1)
        Y = df[output_column]
        Y = np.array(Y).reshape(-1, 1)
        return X, Y

    def plot_actual_vs_predicted(self, actual_column, predicted_column):
        plt.plot(list(range(1, len(actual_column) + 1)), actual_column, color='b', label='actual')
        plt.plot(list(range(1, len(actual_column) + 1)), predicted_column, color='r', label='predicted')
        plt.legend(loc='best')
        plt.suptitle('actual vs. predicted')
        plt.savefig(output_path + 'actual_vs_predicted')
        plt.close()
    
    def plot_actual_vs_predicted_scatter_bisector(self, actual_column, predicted_column):
        fig, ax = plt.subplots()
        ax.scatter(actual_column, predicted_column)
        ax.plot([actual_column.min(), actual_column.max()], [predicted_column.min(), predicted_column.max()], 'k--', lw=4)
        ax.set_xlabel('Actual')
        ax.set_ylabel('Predicted')
        plt.suptitle('actual vs. predicted')
        plt.savefig(output_path + 'actual_vs_predicted_scatter_plot')
        plt.close()
    
    def error_metrics(self, y_test, y_pred, nb_columns, uncertainty = None, epistemic=None, aleatoric=None, heads=None):
        n = len(y_test)
        #convert to float for it to work for AIC and BIC data should all be of the same format i.e float not mix of types
        y_test_f =  [float(item) for item in y_test]
        y_predict_f =  [float(item) for item in y_pred]
        print("shapes in metrics:", y_test.shape, y_pred.shape)
        y_predict_f = y_pred
        test_scores = {}
        r2_Score = r2_score(y_test, y_pred)  # r-squared
        adjusted_r2 = 1 - ((1 - r2_Score) * (n - 1)) / (n - nb_columns - 1)  # adjusted r-squared
        rmse_score = np.sqrt(mean_squared_error(y_test, y_pred))  # RMSE
        mse_score = mean_squared_error(y_test_f, y_predict_f)  # MSE
        mae_score = mean_absolute_error(y_test, y_pred)  # MAE
        mape_score = np.asarray(np.abs(( np.array(y_test) - np.array(y_pred)) / np.array(y_test)), dtype=np.float64).mean() * 100  # MAPE
        re = (mse_score / np.mean(y_pred)) * 100
        accuracy = 100 - np.mean(mape_score)
        spearman_corr, _ = spearmanr(y_test, y_pred)
        r2 = str(round(r2_Score, 2))
        adjusted_r2 = str(round(adjusted_r2, 2))
        mae = str(round(mae_score, 2))
        mse = str(round(mse_score, 2))
        rmse = str(round(rmse_score, 2))
        re = str(round(re, 2))
        spearman = str(round(spearman_corr, 2))
        mape = str(round(mape_score, 2)) + "%"
        accuracy = str(round(accuracy, 2)) + "%"
        
        nmi = 1
        # if len(y_test) == 0 or len(y_pred) == 0:
        #     nmi = 1
        # else:
        #     nmi = normalized_mutual_info_score(np.array(y_test)[0], np.array(y_pred)[0])

        def calculate_aic(n, mse, num_params):
            aic = n * np.log(mse) + 2 * num_params
            return aic

        def calculate_bic(n, mse, num_params):
            bic = n * np.log(mse) + num_params * np.log(n)
            return bic

        # aic = str(round(calculate_aic(len(y_test), mse_score, nb_param), 2))
        # bic = str(round(calculate_bic(len(y_test), mse_score, nb_param), 2))

        test_scores["average output"] = str(y_test.mean())
        test_scores["R2"] = r2
        test_scores["Adjusted R2"] = adjusted_r2
        test_scores["MAE"] = mae
        test_scores["MSE"] = mse
        test_scores["RMSE"] = rmse
        test_scores["Relative Error"] = re
        test_scores["Spearman"] = spearman
        test_scores["MAPE"] = mape
        test_scores["Accuracy"] = accuracy
        test_scores["Information Mutal Normalised"] = nmi
        test_scores["Uncertainty"] = uncertainty
        test_scores["Epistemic Uncertainty"] = epistemic
        test_scores["Aleatoric Uncertainty"] = aleatoric
        test_scores["Heads"] = heads
        # test_scores["AIC"] = aic
        # test_scores["BIC"] = bic
        return test_scores

    def split_train_test(self, df, TRAIN_RATIO):
        X_train = pd.DataFrame()
        X_test = pd.DataFrame()
        Y_train = pd.DataFrame()
        Y_test = pd.DataFrame()
        unique_sites = df["Site Id"].unique()
        print("Number of sites:", len(unique_sites))
        for site in unique_sites:
            df_site = df[df["Site Id"] == site]
            X = df_site
            train_index = int(X.shape[0] * TRAIN_RATIO)

            X_train = X_train.append(X[:train_index], ignore_index = True)
            X_test = X_test.append(X[train_index:], ignore_index = True)
            Y_train = Y_train.append(X[:train_index], ignore_index = True)
            Y_test = Y_test.append(X[train_index:], ignore_index = True)

        Y_train = Y_train[[output_column]]
        Y_test = Y_test[[output_column]]
    
        X_train = X_train.drop([output_column], axis = 1)
        X_test = X_test.drop([output_column], axis = 1)
    
        return X_train, Y_train, X_test, Y_test

    def export_scores(self, filename, scores, columnName):
        file_name = filename
        if not os.path.exists(file_name):
            df = pd.DataFrame(list())
            df.to_csv(file_name, index=False)
        else:
            df = pd.read_csv(file_name, delimiter=',')
        print(scores.keys())
        df["Error Metrics"] = scores.keys()
        df[columnName] = scores.values()
        df.to_csv(file_name, index=False)
        return df
    
