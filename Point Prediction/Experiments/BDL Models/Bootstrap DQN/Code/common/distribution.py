from fitter import Fitter
from fitter import get_common_distributions

from dataset_preparation import DataPreparation
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# Load data set
dataPreparation = DataPreparation()
df = dataPreparation.read_data()
num_rows = df.shape[0]

X_train, Y_train, X_test, Y_test, X_test_exp = dataPreparation.process_train_test_data(df, scaleMinMax=False)
print("X train", X_train.shape, "X test", X_test.shape)
print("Y train", Y_train.shape, "Y test", Y_test.shape)

observed_variable = dataPreparation.output_column

# Create box plot for target
sns.boxplot(x=df[observed_variable])
plt.show()
plt.close()

# Check interquartile range
Q1 = df.quantile(0.25)
Q3 = df.quantile(0.75)
IQR = Q3 - Q1
print(IQR)

# Remove outliers outside the interquartile range
df_non_out = df[~((df < (Q1 - 1.5 * IQR)) |(df > (Q3 + 1.5 * IQR))).any(axis=1)]
print(df_non_out.shape)

# Create box plot for target after outliers removal
sns.boxplot(x=df_non_out[observed_variable])
plt.show()
plt.close()

def density_plot(df, metric):
    '''
    Creates a density plot for the supplied data frame and target
    '''
    sns.distplot(df[metric],
                     hist=False,
                     kde=True, 
                     rug=False,
                     color='darkblue', 
                     kde_kws={'linewidth': 4, 'shade': True},
                     rug_kws={'color': 'black'},
                     label=metric)
    plt.title('Density Plot for ' + metric)
    plt.xlabel(metric)
    plt.ylabel('Density')
    plt.show()
    plt.close()
 

# Create a density plot before and after outlier removal
density_plot(df, observed_variable)
density_plot(df_non_out, observed_variable)


columnsToFit = ["WS", "RH", "TA", "EEflux NDVI", "EEflux Albedo", "EEflux LST",
                observed_variable]
distributionsToFit = get_common_distributions() + ['powerlognorm']
print("Common Distributions are:", distributionsToFit)

# Reference: https://fitter.readthedocs.io/en/latest/_modules/fitter/fitter.html
def fit_distributions(column):
    f = Fitter(df[column], distributions=distributionsToFit, verbose=False)
    f.fit()
    print(f.summary()[["sumsquare_error", "aic", "bic"]])
    print("Best Fit Method for:", column, "is: " f.get_best())
    plt.title('Different Fit Distributions for ' + column) 
    plt.show()
    plt.close()
  
for column in columnsToFit:
    fit_distributions(column)

# Best distribution for the target variable is exponential power