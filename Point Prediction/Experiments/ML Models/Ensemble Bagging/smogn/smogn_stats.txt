Total Number of rare cases: 836 out of 5123
Percentage of Rare Cases: 16.32%

number of rare in train: 667/4090
==> 16.3080684596577%%
number of rare in test: 169/1033
==> 16.36011616650532%%
The size of the original data is (4090, 49)
The size of the oversampled data is (4219, 49)
Total Number of rare cases: 777 out of 4219
Percentage of Rare Cases: 18.42%

The percentage of rare values in dataset after smogn are 18.416686418582604
