
Utility Based Metrics
F1: 0.80823
F2: 0.78099
F05: 0.83744
precision: 0.85812
recall: 0.76382

Regression Error Metrics
R2: 0.60777
Adj-R2: 0.58780
MSE: 2.04175
MAE: 1.07465
MAPE: 36.27434

Correlations
Pearson: 0.78209
Spearman: 0.76549
Distance: 0.21791

Utility Based Metrics
F1: 0.80495
F2: 0.78474
F05: 0.82622
precision: 0.84105
recall: 0.77182

Regression Error Metrics
R2: 0.60576
Adj-R2: 0.58569
MSE: 2.05220
MAE: 1.07691
MAPE: 36.87549

Correlations
Pearson: 0.78038
Spearman: 0.76199
Distance: 0.21962

Utility Based Metrics
F1: 0.80374
F2: 0.78535
F05: 0.82303
precision: 0.83640
recall: 0.77354

Regression Error Metrics
R2: 0.61259
Adj-R2: 0.59287
MSE: 2.01663
MAE: 1.07151
MAPE: 36.67310

Correlations
Pearson: 0.78560
Spearman: 0.76717
Distance: 0.21440

Utility Based Metrics
F1: 0.82046
F2: 0.78930
F05: 0.85418
precision: 0.87824
recall: 0.76981

Regression Error Metrics
R2: 0.60782
Adj-R2: 0.58785
MSE: 2.04151
MAE: 1.07397
MAPE: 36.88665

Correlations
Pearson: 0.78199
Spearman: 0.76236
Distance: 0.21801

Utility Based Metrics
F1: 0.80354
F2: 0.78360
F05: 0.82451
precision: 0.83912
recall: 0.77085

Regression Error Metrics
R2: 0.60868
Adj-R2: 0.58875
MSE: 2.03702
MAE: 1.07452
MAPE: 36.82454

Correlations
Pearson: 0.78247
Spearman: 0.76093
Distance: 0.21753

Utility Based Metrics
F1: 0.79408
F2: 0.78039
F05: 0.80827
precision: 0.81801
recall: 0.77151

Regression Error Metrics
R2: 0.60724
Adj-R2: 0.58724
MSE: 2.04449
MAE: 1.07605
MAPE: 37.60834

Correlations
Pearson: 0.78267
Spearman: 0.75555
Distance: 0.21733

Utility Based Metrics
F1: 0.79989
F2: 0.79277
F05: 0.80714
precision: 0.81204
recall: 0.78810

Regression Error Metrics
R2: 0.61017
Adj-R2: 0.59032
MSE: 2.02927
MAE: 1.07506
MAPE: 37.37219

Correlations
Pearson: 0.78383
Spearman: 0.76125
Distance: 0.21617

Utility Based Metrics
F1: 0.79570
F2: 0.78457
F05: 0.80714
precision: 0.81496
recall: 0.77732

Regression Error Metrics
R2: 0.61441
Adj-R2: 0.59478
MSE: 2.00718
MAE: 1.06328
MAPE: 36.28335

Correlations
Pearson: 0.78583
Spearman: 0.76847
Distance: 0.21417

Utility Based Metrics
F1: 0.79619
F2: 0.78332
F05: 0.80949
precision: 0.81861
recall: 0.77497

Regression Error Metrics
R2: 0.62140
Adj-R2: 0.60213
MSE: 1.97078
MAE: 1.05010
MAPE: 35.84879

Correlations
Pearson: 0.79055
Spearman: 0.77161
Distance: 0.20945

Utility Based Metrics
F1: 0.81139
F2: 0.79212
F05: 0.83161
precision: 0.84566
recall: 0.77978

Regression Error Metrics
R2: 0.62175
Adj-R2: 0.60249
MSE: 1.96897
MAE: 1.06513
MAPE: 36.68755

Correlations
Pearson: 0.79139
Spearman: 0.76965
Distance: 0.20861

Utility Based Metrics
F1: 0.79542
F2: 0.77623
F05: 0.81559
precision: 0.82961
recall: 0.76394

Regression Error Metrics
R2: 0.59965
Adj-R2: 0.57926
MSE: 2.08403
MAE: 1.09205
MAPE: 37.61112

Correlations
Pearson: 0.77810
Spearman: 0.76200
Distance: 0.22190

Utility Based Metrics
F1: 0.82327
F2: 0.78786
F05: 0.86200
precision: 0.88992
recall: 0.76590

Regression Error Metrics
R2: 0.61924
Adj-R2: 0.59985
MSE: 1.98204
MAE: 1.07830
MAPE: 37.67396

Correlations
Pearson: 0.79391
Spearman: 0.76823
Distance: 0.20609

Utility Based Metrics
F1: 0.81699
F2: 0.78835
F05: 0.84779
precision: 0.86965
recall: 0.77035

Regression Error Metrics
R2: 0.60815
Adj-R2: 0.58820
MSE: 2.03976
MAE: 1.07245
MAPE: 36.59261

Correlations
Pearson: 0.78265
Spearman: 0.76975
Distance: 0.21735

Utility Based Metrics
F1: 0.80687
F2: 0.76006
F05: 0.85983
precision: 0.89917
recall: 0.73175

Regression Error Metrics
R2: 0.51470
Adj-R2: 0.48999
MSE: 2.52621
MAE: 1.23319
MAPE: 44.67314

Correlations
Pearson: 0.72836
Spearman: 0.70550
Distance: 0.27164

Utility Based Metrics
F1: 0.81225
F2: 0.79537
F05: 0.82987
precision: 0.84204
recall: 0.78450

Regression Error Metrics
R2: 0.61808
Adj-R2: 0.59864
MSE: 1.98806
MAE: 1.05743
MAPE: 36.54879

Correlations
Pearson: 0.78864
Spearman: 0.76722
Distance: 0.21136

Utility Based Metrics
F1: 0.80747
F2: 0.76626
F05: 0.85337
precision: 0.88698
recall: 0.74105

Regression Error Metrics
R2: 0.55994
Adj-R2: 0.53753
MSE: 2.29071
MAE: 1.16435
MAPE: 40.86208

Correlations
Pearson: 0.75600
Spearman: 0.73569
Distance: 0.24400

Utility Based Metrics
F1: 0.80942
F2: 0.79081
F05: 0.82892
precision: 0.84245
recall: 0.77888

Regression Error Metrics
R2: 0.61117
Adj-R2: 0.59137
MSE: 2.02406
MAE: 1.06871
MAPE: 36.61446

Correlations
Pearson: 0.78350
Spearman: 0.76048
Distance: 0.21650

Utility Based Metrics
F1: 0.75038
F2: 0.71255
F05: 0.79245
precision: 0.82321
recall: 0.68938

Regression Error Metrics
R2: 0.46520
Adj-R2: 0.43797
MSE: 2.78387
MAE: 1.30575
MAPE: 47.54434

Correlations
Pearson: 0.71262
Spearman: 0.69106
Distance: 0.28738

Utility Based Metrics
F1: 0.82839
F2: 0.79954
F05: 0.85940
precision: 0.88140
recall: 0.78139

Regression Error Metrics
R2: 0.62330
Adj-R2: 0.60412
MSE: 1.96091
MAE: 1.06123
MAPE: 36.60980

Correlations
Pearson: 0.79248
Spearman: 0.76870
Distance: 0.20752

Utility Based Metrics
F1: 0.80001
F2: 0.79180
F05: 0.80838
precision: 0.81406
recall: 0.78643

Regression Error Metrics
R2: 0.61808
Adj-R2: 0.59863
MSE: 1.98808
MAE: 1.03554
MAPE: 33.81950

Correlations
Pearson: 0.78686
Spearman: 0.78071
Distance: 0.21314

Utility Based Metrics
F1: 0.81306
F2: 0.79705
F05: 0.82972
precision: 0.84121
recall: 0.78673

Regression Error Metrics
R2: 0.61440
Adj-R2: 0.59477
MSE: 2.00722
MAE: 1.06833
MAPE: 36.41520

Correlations
Pearson: 0.78545
Spearman: 0.76458
Distance: 0.21455

Utility Based Metrics
F1: 0.80343
F2: 0.78993
F05: 0.81741
precision: 0.82700
recall: 0.78117

Regression Error Metrics
R2: 0.60549
Adj-R2: 0.58541
MSE: 2.05359
MAE: 1.07363
MAPE: 36.66360

Correlations
Pearson: 0.78024
Spearman: 0.76502
Distance: 0.21976

Utility Based Metrics
F1: 0.80977
F2: 0.78921
F05: 0.83144
precision: 0.84654
recall: 0.77607

Regression Error Metrics
R2: 0.61411
Adj-R2: 0.59446
MSE: 2.00876
MAE: 1.05825
MAPE: 36.29684

Correlations
Pearson: 0.78555
Spearman: 0.76404
Distance: 0.21445

Utility Based Metrics
F1: 0.80518
F2: 0.79154
F05: 0.81930
precision: 0.82899
recall: 0.78271

Regression Error Metrics
R2: 0.62409
Adj-R2: 0.60495
MSE: 1.95680
MAE: 1.05621
MAPE: 36.47818

Correlations
Pearson: 0.79271
Spearman: 0.77003
Distance: 0.20729

Utility Based Metrics
F1: 0.81365
F2: 0.79246
F05: 0.83601
precision: 0.85161
recall: 0.77894

Regression Error Metrics
R2: 0.60566
Adj-R2: 0.58558
MSE: 2.05273
MAE: 1.07441
MAPE: 37.06013

Correlations
Pearson: 0.78083
Spearman: 0.76121
Distance: 0.21917

Utility Based Metrics
F1: 0.81027
F2: 0.79103
F05: 0.83047
precision: 0.84450
recall: 0.77871

Regression Error Metrics
R2: 0.60766
Adj-R2: 0.58768
MSE: 2.04231
MAE: 1.05942
MAPE: 35.98576

Correlations
Pearson: 0.78107
Spearman: 0.76798
Distance: 0.21893

Utility Based Metrics
F1: 0.81015
F2: 0.79261
F05: 0.82848
precision: 0.84117
recall: 0.78134

Regression Error Metrics
R2: 0.61463
Adj-R2: 0.59501
MSE: 2.00604
MAE: 1.05842
MAPE: 36.42310

Correlations
Pearson: 0.78594
Spearman: 0.76716
Distance: 0.21406

Utility Based Metrics
F1: 0.80243
F2: 0.78565
F05: 0.81993
precision: 0.83203
recall: 0.77486

Regression Error Metrics
R2: 0.59848
Adj-R2: 0.57804
MSE: 2.09009
MAE: 1.08018
MAPE: 37.32518

Correlations
Pearson: 0.77615
Spearman: 0.75935
Distance: 0.22385

Utility Based Metrics
F1: 0.80883
F2: 0.79363
F05: 0.82463
precision: 0.83551
recall: 0.78381

Regression Error Metrics
R2: 0.61808
Adj-R2: 0.59864
MSE: 1.98805
MAE: 1.05912
MAPE: 36.75139

Correlations
Pearson: 0.78932
Spearman: 0.76811
Distance: 0.21068

Utility Based Metrics
F1: 0.77371
F2: 0.77057
F05: 0.77688
precision: 0.77901
recall: 0.76849

Regression Error Metrics
R2: 0.56264
Adj-R2: 0.54037
MSE: 2.27668
MAE: 1.13620
MAPE: 39.51743

Correlations
Pearson: 0.75335
Spearman: 0.74546
Distance: 0.24665

Utility Based Metrics
F1: 0.81242
F2: 0.79582
F05: 0.82973
precision: 0.84168
recall: 0.78513

Regression Error Metrics
R2: 0.61690
Adj-R2: 0.59739
MSE: 1.99423
MAE: 1.06201
MAPE: 36.68972

Correlations
Pearson: 0.78720
Spearman: 0.76174
Distance: 0.21280

Utility Based Metrics
F1: 0.80251
F2: 0.78625
F05: 0.81946
precision: 0.83117
recall: 0.77577

Regression Error Metrics
R2: 0.59178
Adj-R2: 0.57099
MSE: 2.12500
MAE: 1.09568
MAPE: 37.94652

Correlations
Pearson: 0.77178
Spearman: 0.75429
Distance: 0.22822

Utility Based Metrics
F1: 0.80465
F2: 0.78453
F05: 0.82583
precision: 0.84058
recall: 0.77167

Regression Error Metrics
R2: 0.59844
Adj-R2: 0.57799
MSE: 2.09031
MAE: 1.08497
MAPE: 36.73143

Correlations
Pearson: 0.77535
Spearman: 0.76385
Distance: 0.22465

Utility Based Metrics
F1: 0.81726
F2: 0.79842
F05: 0.83702
precision: 0.85073
recall: 0.78633

Regression Error Metrics
R2: 0.60724
Adj-R2: 0.58724
MSE: 2.04449
MAE: 1.07528
MAPE: 37.50649

Correlations
Pearson: 0.78228
Spearman: 0.76145
Distance: 0.21772

Utility Based Metrics
F1: 0.81780
F2: 0.79673
F05: 0.84002
precision: 0.85551
recall: 0.78327

Regression Error Metrics
R2: 0.61244
Adj-R2: 0.59271
MSE: 2.01741
MAE: 1.06752
MAPE: 36.91311

Correlations
Pearson: 0.78509
Spearman: 0.76548
Distance: 0.21491

Utility Based Metrics
F1: 0.80826
F2: 0.78647
F05: 0.83129
precision: 0.84739
recall: 0.77258

Regression Error Metrics
R2: 0.60676
Adj-R2: 0.58674
MSE: 2.04701
MAE: 1.08000
MAPE: 36.92753

Correlations
Pearson: 0.78145
Spearman: 0.76383
Distance: 0.21855

Utility Based Metrics
F1: 0.79535
F2: 0.78890
F05: 0.80191
precision: 0.80634
recall: 0.78466

Regression Error Metrics
R2: 0.60863
Adj-R2: 0.58871
MSE: 2.03725
MAE: 1.06688
MAPE: 36.76459

Correlations
Pearson: 0.78198
Spearman: 0.75910
Distance: 0.21802

Utility Based Metrics
F1: 0.79824
F2: 0.78771
F05: 0.80905
precision: 0.81642
recall: 0.78085

Regression Error Metrics
R2: 0.60700
Adj-R2: 0.58699
MSE: 2.04575
MAE: 1.07654
MAPE: 36.92620

Correlations
Pearson: 0.78105
Spearman: 0.76084
Distance: 0.21895

Utility Based Metrics
F1: 0.80811
F2: 0.79179
F05: 0.82511
precision: 0.83686
recall: 0.78127

Regression Error Metrics
R2: 0.61440
Adj-R2: 0.59477
MSE: 2.00720
MAE: 1.06462
MAPE: 36.38892

Correlations
Pearson: 0.78581
Spearman: 0.76643
Distance: 0.21419

Utility Based Metrics
F1: 0.80297
F2: 0.79238
F05: 0.81385
precision: 0.82127
recall: 0.78547

Regression Error Metrics
R2: 0.61446
Adj-R2: 0.59483
MSE: 2.00690
MAE: 1.06505
MAPE: 36.51204

Correlations
Pearson: 0.78589
Spearman: 0.76677
Distance: 0.21411

Utility Based Metrics
F1: 0.81039
F2: 0.78923
F05: 0.83271
precision: 0.84829
recall: 0.77573

Regression Error Metrics
R2: 0.60731
Adj-R2: 0.58732
MSE: 2.04414
MAE: 1.06741
MAPE: 36.43031

Correlations
Pearson: 0.78124
Spearman: 0.76426
Distance: 0.21876

Utility Based Metrics
F1: 0.81730
F2: 0.79105
F05: 0.84535
precision: 0.86515
recall: 0.77446

Regression Error Metrics
R2: 0.60674
Adj-R2: 0.58671
MSE: 2.04711
MAE: 1.07936
MAPE: 36.92200

Correlations
Pearson: 0.78114
Spearman: 0.76341
Distance: 0.21886

Utility Based Metrics
F1: 0.81540
F2: 0.79875
F05: 0.83275
precision: 0.84474
recall: 0.78803

Regression Error Metrics
R2: 0.61193
Adj-R2: 0.59217
MSE: 2.02010
MAE: 1.06517
MAPE: 36.95295

Correlations
Pearson: 0.78440
Spearman: 0.76003
Distance: 0.21560

Utility Based Metrics
F1: 0.80903
F2: 0.78904
F05: 0.83006
precision: 0.84470
recall: 0.77625

Regression Error Metrics
R2: 0.60089
Adj-R2: 0.58057
MSE: 2.07756
MAE: 1.07845
MAPE: 37.00391

Correlations
Pearson: 0.77693
Spearman: 0.75816
Distance: 0.22307

Utility Based Metrics
F1: 0.80779
F2: 0.79163
F05: 0.82463
precision: 0.83625
recall: 0.78121

Regression Error Metrics
R2: 0.61243
Adj-R2: 0.59269
MSE: 2.01750
MAE: 1.06588
MAPE: 36.71472

Correlations
Pearson: 0.78490
Spearman: 0.76482
Distance: 0.21510

Utility Based Metrics
F1: 0.81226
F2: 0.79178
F05: 0.83384
precision: 0.84887
recall: 0.77868

Regression Error Metrics
R2: 0.61040
Adj-R2: 0.59056
MSE: 2.02804
MAE: 1.06980
MAPE: 36.96088

Correlations
Pearson: 0.78380
Spearman: 0.76374
Distance: 0.21620

Utility Based Metrics
F1: 0.81352
F2: 0.79222
F05: 0.83600
precision: 0.85168
recall: 0.77863

Regression Error Metrics
R2: 0.60739
Adj-R2: 0.58740
MSE: 2.04371
MAE: 1.06784
MAPE: 36.31080

Correlations
Pearson: 0.78090
Spearman: 0.76621
Distance: 0.21910
