
Utility Based Metrics
F1: 0.80967
F2: 0.79296
F05: 0.82711
precision: 0.83915
recall: 0.78219

Regression Error Metrics
R2: 0.61334
Adj-R2: 0.59407
RMSE: 1.41871
MSE: 2.01274
MAE: 1.04319
MAPE: 34.75373

Correlations
Pearson: 0.78334
Spearman: 0.75487
Distance: 0.21666
