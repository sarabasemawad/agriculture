
Utility Based Metrics
F1: 0.00001
F2: 0.00001
F05: 0.00001
precision: 0.00001
recall: 0.00001

Regression Error Metrics
R2: 0.41213
Adj-R2: 0.31815
MSE: 3.72887
MAE: 1.52077
MAPE: 37.69056

Correlations
Pearson: 0.64504
Spearman: 0.62613
Distance: 0.35496
