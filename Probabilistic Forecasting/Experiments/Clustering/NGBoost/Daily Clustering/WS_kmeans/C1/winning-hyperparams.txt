{'Dist': <class 'ngboost.distns.exponential.Exponential'>, 'n_estimators': 500, 'Base': DecisionTreeRegressor(criterion='friedman_mse', max_depth=2,
           max_features=None, max_leaf_nodes=None,
           min_impurity_decrease=0.0, min_impurity_split=None,
           min_samples_leaf=1, min_samples_split=2,
           min_weight_fraction_leaf=0.0, presort=False, random_state=None,
           splitter='best'), 'learning_rate': 0.1, 'minibatch_frac': 1.0, 'verbose': False}