import mixture_model

import tensorflow_probability as tfp
from tensorflow_probability import distributions as tfd
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 
import numpy as np


import sys
sys.path.insert(1, '/Users/saraawad/Desktop/Modules Code/Uncertainty/MCDO-master/common/')

from dataset_preparation import DataPreparation

def mixture_training(x_truth, y_truth, dropout, learning_rate, epochs, n_mixtures, display_step=10):
    """
    Generic training of a Mixture Density Mixture Network for 2D data.

    :param x_truth: training samples x
    :param y_truth: training samples y / label
    :param dropout:
    :param learning_rate:
    :param epochs:
    :param n_mixtures: Number of mixtures in GMM
    :param display_step:
    :return: session, x_placeholder, dropout_placeholder
    """
    tf.reset_default_graph()
    x_placeholder = tf.placeholder(tf.float64, [None, x_truth.shape[1]])
    y_placeholder = tf.placeholder(tf.float64, [None, 1])
    dropout_placeholder = tf.placeholder(tf.float64)
    eps = 1e-4

    gmm, mean, uncertainties = mixture_model.mixture_model(x_placeholder, dropout_placeholder, n_mixtures=n_mixtures)

    tf.add_to_collection("gmm", gmm)
    tf.add_to_collection("prediction", mean)
    tf.add_to_collection("uncertainties", uncertainties)

    mixture_weights = gmm[0]
    mixture_means = gmm[1]
    mixture_variances = gmm[2]
    # TODO: should check for distribution and use this distribution
    #Default normal distribution
    # dist = tfp.distributions.Normal(loc=mixture_means, scale=mixture_variances)
    # power = np.ones(shape=mixture_variances.shape)
    #Use the exponential pow function since it was the best fitter for the output
    dist = tfp.distributions.GeneralizedNormal(loc=mixture_means, scale=mixture_variances, power=1)
    # Negative logarithmic likelihood
    loss = - tf.reduce_mean(
        tf.log(tf.reduce_sum(mixture_weights * dist.prob(y_placeholder), axis=1) + eps),
        axis=0
    )
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train = optimizer.minimize(loss)

    init = tf.global_variables_initializer()
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=False))
    sess.run(init)

    for epoch in range(epochs):
        feed_dict = {x_placeholder: x_truth,
                     y_placeholder: y_truth,
                     dropout_placeholder: dropout}

        sess.run(train, feed_dict=feed_dict)

        if epoch % display_step == 0:
            print("Epoch {}".format(epoch))
            current_loss = sess.run(loss, feed_dict=feed_dict)
            print("Loss {}".format(current_loss))
            print("================")

    print("Training done")


    return sess, x_placeholder, dropout_placeholder


if __name__ == "__main__":
     # Load data set
    dataPreparation = DataPreparation()
    df = dataPreparation.read_data()
    num_rows = df.shape[0]

    X_train, Y_train, X_test, Y_test, X_test_exp = dataPreparation.process_train_test_data(df, scaleMinMax=False)
    print("X train", X_train.shape, "X test", X_test.shape)
    print("Y train", Y_train.shape, "Y test", Y_test.shape)

    mixture_training(X_train, Y_train, 0.3, 1e-3, 1000, 1) #TODO: see why there were no nmixtures provided




