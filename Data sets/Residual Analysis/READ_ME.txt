This folder contains 4 datasets used for the Residual Analysis Experiments:

-joint_library_cleaned.csv: Joint Library dataset with duplicates removed and columns not needed in training dropped (all but encoded and weather)
-joint_manual_cleaned.csv: Joint Manual dataset with duplicates removed and columns not needed in training dropped (all but encoded and weather)
