This folder contains two Datasets:

-library_joint_cleaned_final.csv: Library joint dataset with duplicate removal.
-manual_joint_cleaned_final.csv: Manual joint dataset with duplicate removal.