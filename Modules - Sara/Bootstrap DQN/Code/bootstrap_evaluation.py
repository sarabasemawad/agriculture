from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import json
import time
import warnings
warnings.filterwarnings("ignore")

import sys
sys.path.insert(1, '/Users/saraawad/Desktop/Modules Code/Uncertainty/MCDO-master/common/')

from bootstrap_training import bootstrap_training
from dataset_preparation import DataPreparation
import plotting


def bootstrap_evaluation(X_train, Y_train, X_test, Y_test, X_test_exp, dropout, learning_rate, epochs, n_heads, num_rows, ax):
    sess, x_placeholder, dropout_placeholder, mask_placeholder = bootstrap_training(X_train, Y_train, dropout, learning_rate, epochs, n_heads)

    prediction_op = sess.graph.get_collection("prediction")
    uncertainty_op = sess.graph.get_collection("uncertainties")
    heads_op = sess.graph.get_collection("heads")

    feed_dict = {x_placeholder: X_test,
                 dropout_placeholder: 0,
                 mask_placeholder: np.ones(shape=(len(X_test), n_heads, 1))}

    predictions, uncertainties_eval, heads_eval = sess.run([prediction_op, uncertainty_op, heads_op], feed_dict)
    heads_eval = np.array(heads_eval).reshape(len(X_test), n_heads)
    
    Y_predict = predictions[0].flatten()
    uncertainties_eval = uncertainties_eval[0].flatten()

     # Get point predictions by averging predictions
    Y_predict_average = np.mean(heads_eval, axis=1).flatten()

    # Export test data set with predicted
    dataset_preparation = DataPreparation()
    dataPreparation.export_test_dataset(X_test_exp, Y_predict_average, [uncertainties_eval], ['Uncertainty'])

    # Remove un-used columns
    output_path = dataPreparation.output_path
    columns = []
    columns.extend(df.columns)
    columns.remove("Site Id")
    columns.remove("Date")
    columns.remove(dataPreparation.output_column)
    # print("Columns", columns, len(columns))
    num_columns = len(columns)
    average_output = np.mean(df['LE_bowen_corr(mm)'])

    # Calculate error metrics
    Y_test.dropna(inplace=True)
    Y_test = np.array(Y_test)

    # print("uncertainty:", uncertainties_eval, uncertainties_eval.shape)
    dataset_preparation = DataPreparation()
    with open(dataset_preparation.output_path + "uncertainty.txt", 'w') as filehandle:
        json.dump(np.array(uncertainties_eval).tolist(), filehandle)


    Y_predict_average = np.array(Y_predict_average).reshape(-1, 1)
    metrics = dataset_preparation.error_metrics(Y_test, Y_predict_average, average_output, num_rows, num_columns, uncertainties_eval)
    # print("metrics:", metrics)
    dataset_preparation.export_scores(output_path + "error_head_" + str(n_heads) + ".csv", metrics, "Testing Avg")

    for i in range(n_heads):
        y_predict_head = np.array(heads_eval[:, i]).reshape(-1, 1)
        metrics = dataset_preparation.error_metrics(Y_test, y_predict_head, average_output, num_rows, num_columns, uncertainties_eval)
        # print("metrics for head:", str(i+1), metrics)
        dataset_preparation.export_scores(output_path + "error_head_" + str(n_heads) + ".csv", metrics, "Testing Head " + str(i+1))

    n_samples = 100
    # for i in range(n_heads):
    #     ax.plot(X_test[:, 0], heads_eval[:, i], alpha=0.3)

    file_name = dataset_preparation.output_path + "head_" + str(n_heads)
    plotting.plot_mean_vs_truth(X_test, Y_test, X_test, Y_predict_average, uncertainties_eval, ax, n_samples, file_name)

if __name__ == "__main__":
    # Hyper parameters
    heads = [3, 5, 10, 15]
    epochs = 1200
    #15000
    learningRate = 1e-3
    dropout = 0.2

    fig, axs = plt.subplots(len(heads), 1, figsize=(30, 5*len(heads)), sharey=True)
    fig.suptitle('Bootstrap-Model | Epochs:' +  str(epochs) + 'Learning Rate:' + str(learningRate), fontsize=20)

    # Load data set
    dataPreparation = DataPreparation()
    df = dataPreparation.read_data()
    num_rows = df.shape[0]

    X_train, Y_train, X_test, Y_test, X_test_exp = dataPreparation.process_train_test_data(df, scaleMinMax=True)
    # print("X train", X_train.shape, "X test", X_test.shape)
    # print("Y train", Y_train.shape, "Y test", Y_test.shape)

    for n_heads, ax in zip(heads, axs):
        ax.set_title("%d Heads" % n_heads)
       
        bootstrap_evaluation(X_train, Y_train, X_test, Y_test, X_test_exp, dropout, learningRate, epochs, n_heads, num_rows, ax)
        fig.savefig(dataPreparation.output_path + "bootstrap_heads.pdf")