import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
import os

from dataset_preparation import DataPreparation
from mc_model import Net


from tensorflow.keras.layers import Input, Dense, Dropout
from tensorflow.keras import Sequential
from tensorflow.keras.layers import BatchNormalization
from keras.regularizers import l2
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint

import warnings
warnings.filterwarnings("ignore")

if __name__ == "__main__": 

    # Load hyper parameters
#     dropout_rates = [0.01, 0.05, 0.1, 0.15, 0.2]
    dropout_rates = [0.3]
#     tau_values = [0.1, 0.15, 0.2, 1]
    tau_values = [0.15]
    num_hidden_layers = 2
    n_hidden = 64
    n_epochs = 500
    epochs_multiplier = 1
    
    dataPreparation = DataPreparation()
    df = dataPreparation.read_data()

    output_path = dataPreparation.output_path
    columns = []
    columns.extend(df.columns)
    columns.remove("Site Id")
    columns.remove("Date")
    columns.remove(dataPreparation.output_column)
    print("Columns", columns, len(columns))
    num_columns = len(columns)
    
    average_output = np.mean(df['LE_bowen_corr(mm)'])
    rel_points = np.array([[1, 0 , 0], [4, 0 , 0], [15, 1 , 0]])

    X_train, Y_train, X_test, Y_test, X_valid, Y_valid, X_test_exp = dataPreparation.process_data(df)
    print("X train", X_train.shape, "X test", X_test.shape, "X valid", X_valid.shape)
    print("Y train", Y_train.shape, "Y test", Y_test.shape, "Y valid", Y_valid.shape)

    # We perform grid-search to select the best hyperparameters based on the highest log-likelihood value
    best_network = None
    best_ll = -float('inf')
    best_tau = 0
    best_dropout = 0
    index = 0
    for dropout_rate in dropout_rates:
        for tau in tau_values:
            print ('Grid search step: Tau: ' + str(tau) + ' Dropout rate: ' + str(dropout_rate))
            network = Net(X_train, Y_train, X_valid, Y_valid, ([ int(n_hidden) ] * num_hidden_layers),
                    normalize = True, n_epochs = int(n_epochs * epochs_multiplier), tau = tau,
                    dropout = dropout_rate)
           
            index += 1
            dataPreparation.plot_learning_curve(network.history, index)

            # We obtain the test RMSE and the test ll from the validation sets
            y_pred, y_hat, MC_error, ll, MC_pred, predictive_variance = network.predict(X_valid, Y_valid)
            if (ll > best_ll):
                best_ll = ll
                best_network = network
                best_tau = tau
                best_dropout = dropout_rate
                print ('Best log_likelihood changed to: ' + str(best_ll))
                print ('Best tau changed to: ' + str(best_tau))
                print ('Best dropout rate changed to: ' + str(best_dropout))
            
            # Storing validation results
            Y_valid.dropna(inplace=True)
            y_pred = np.array(y_pred).reshape(-1, 1)
            print("shapes:", Y_valid.shape, y_pred.shape)
            metrics = dataPreparation.evaluate(average_output, len(Y_valid), 48, np.array(Y_valid), np.array(y_pred),
                                               network.num_trainable_parameters, 0.1, "range", "high", 1.5, rel_points)
            metrics["Probabilistic RMSE"] = MC_error
            metrics["Probabilistic NLL"] = best_ll
            metrics["Training Time (seconds)"] = "-"
            metrics["Testing Time (seconds)"] = "-"
            #Save results
            dataPreparation.export_scores(output_path + "test_scores.csv", metrics, "Manual Validation")
            print("metrics", metrics)
            with open(output_path + "Valid_Results", "a") as myfile:
                myfile.write('Dropout_Rate: ' + repr(dropout_rate) + ' Tau: ' + repr(tau) + ' :: ')
                myfile.write(repr(metrics) + '\n')

            with open(output_path + "Valid_MC_RMSE_Results", "a") as myfile:
                myfile.write('Dropout_Rate: ' + repr(dropout_rate) + ' Tau: ' + repr(tau) + ' :: ')
                myfile.write(repr(MC_error) + '\n')

            with open(output_path + "Valid_LL_Results", "a") as myfile:
                myfile.write('Dropout_Rate: ' + repr(dropout_rate) + ' Tau: ' + repr(tau) + ' :: ')
                myfile.write(repr(ll) + '\n')

    # Storing test results
    # Get columns and remove un-used ones
    # Get full training set by concatenating train and validation data sets
    X_train_full = pd.concat([pd.DataFrame(X_train, columns=columns), pd.DataFrame(X_valid, columns=columns)], axis=0)
    Y_train_full = pd.concat([Y_train, Y_valid], axis=0)

    print("best params are:", best_tau, best_dropout)
    
    # Save start training time
    start_time = time.time()
    # Run experiment on training data set
    best_network = Net(X_train_full, Y_train_full, None, None, ([ int(n_hidden) ] * num_hidden_layers),
                    normalize = True, n_epochs = int(n_epochs * epochs_multiplier), tau = best_tau,
                    dropout = best_dropout)
    # Save end training time
    training_time = (time.time() - start_time)
    print("Training time in seconds: %.5f seconds" % training_time)
    
    # Save start testing time
    start_time = time.time()
    # Predict on training data set
    y_pred, y_hat, MC_error, ll, MC_pred, predictive_variance = best_network.predict(X_test, Y_test) 
    # Save end testing time
    testing_time = (time.time() - start_time)
    print("Testing time in seconds: %.5f seconds" % testing_time)

    # Export test data set with predicted
    dataPreparation.export_test_dataset(X_test_exp, y_pred)


    # Calculate error metrics
    Y_test.dropna(inplace=True)
    y_pred = np.array(y_pred).reshape(-1, 1)
    print("shapes:", Y_test.shape, y_pred.shape)
    metrics = dataPreparation.evaluate(average_output, len(Y_test), 48, np.array(Y_test), np.array(y_pred),
                                               network.num_trainable_parameters, 0.1, "range", "high", 1.5, rel_points)
    metrics["Probabilistic RMSE"] = MC_error
    metrics["Probabilistic NLL"] = ll
    metrics["Training Time (seconds)"] = training_time
    metrics["Testing Time (seconds)"] = testing_time
    print("final test metrics", metrics)


    # Save results
    dataPreparation.export_scores(output_path + "test_scores.csv", metrics, "Manual Testing")
    with open(output_path + "Test_Results", "a") as myfile:
                myfile.write('Dropout_Rate: ' + repr(best_dropout) + ' Tau: ' + repr(best_tau) + ' :: ')
                myfile.write(repr(metrics) + '\n')

    # Plot scatter plot and actual versus predicted for output for each model
    dataPreparation.plot_actual_vs_predicted(Y_test, y_pred, "test")
    dataPreparation.plot_actual_vs_predicted_scatter_bisector(Y_test, y_pred, "test")
    
    # Plot variance
    dataPreparation.plot_variance(Y_test, y_pred, MC_pred, predictive_variance, 20)

    #Plot mcdropout
    # network.plot_errors_mc_dropout(y_pred, X_test, Y_test, network.T)
    #TODO: computed deviation
#     y_std = np.sqrt(predictive_variance)
#     dataPreparation.plot_regression_model_analysis(gt=None, trn=(X_train[:, 0], Y_train), pred=(X_test[:, 0], y_pred, y_std[:, 0]), title='MC Dropout Regression')


       