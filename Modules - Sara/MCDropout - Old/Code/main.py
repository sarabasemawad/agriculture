import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from dataset_preparation import DataPreparation


from tensorflow.keras.layers import Input, Dense, Dropout
from tensorflow.keras import Sequential
from tensorflow.keras.layers import BatchNormalization
from keras.regularizers import l2
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint

#Blog: https://everyhue.me/posts/why-uncertainty-matters/

class MCDropout():
    def gaussian_nll(self, y_true, y_pred):
        '''
        Gaussian negative log likelihood
        Note: to make training more stable, we optimize
        a modified loss by having our model predict log(sigma^2)
        rather than sigma^2.
        '''
        y_true = tf.reshape(y_true, [-1])
        mu = y_pred[:, 0]
        si = y_pred[:, 1]
        loss = (si + tf.math.squared_difference(y_true, mu)/tf.math.exp(si)) / 2.0
        return tf.reduce_mean(loss)

    def build_model(self, loss):
        model = Sequential([
            Dense(64, kernel_regularizer=l2(0.001), activation='softmax'),
            BatchNormalization(),
            Dropout(.5),
            Dense(64, activation='softmax'),
            Dropout(.5),
            Dense(2, activation=None),
        ])
        model.compile(loss=loss, optimizer='adam', metrics=['mse', 'mae', 'accuracy', 'mape'])
        return model

    def train(self, x, y, x_val, y_val, model, batch_size=32, epochs=100):
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
        mc = ModelCheckpoint("test_experiment.h5", monitor='val_mape', mode='min', verbose=1,
         save_best_only=True)
        model.fit(x, y, validation_data=(x_val, y_val), callbacks=[es, mc], batch_size=batch_size, epochs=epochs, verbose=1)
        return model

    def predict(self, model, x, T=20):
        '''
        Args:
            model: The trained keras model
            x: the input tensor with shape [N, M]
            T: the number of monte carlo trials to sample
        Returns:
            y_mean: The expected value of our prediction
            y_std: The standard deviation of our prediction
        '''
        mu_arr = []
        si_arr = []

        for t in range(T):
            y_pred = model(x, training=True)
            mu = y_pred[:, 0]
            si = y_pred[:, 1]

            mu_arr.append(mu)
            si_arr.append(si)

        mu_arr = np.array(mu_arr)
        si_arr = np.array(si_arr)
        var_arr = np.exp(si_arr)

        y_mean = np.mean(mu_arr, axis=0)
        y_variance = np.mean(var_arr + mu_arr**2, axis=0) - y_mean**2
        y_std = np.sqrt(y_variance)
        return y_mean, y_std

if __name__ == "__main__":
    dataPreparation = DataPreparation()
    df = dataPreparation.read_data()

    X_train_scaled, Y_train, X_test_scaled, Y_test, X_valid_scaled, Y_valid = dataPreparation.process_data(df)
    print("X train", X_train_scaled.shape, "X test", X_test_scaled.shape, "X valid", X_valid_scaled.shape)
    print("Y train", Y_train.shape, "Y test", Y_test.shape, "Y valid", Y_valid.shape)

    mc = MCDropout()
    # Create NLL loss
    loss = mc.gaussian_nll
    # Build model architecture
    model = mc.build_model(loss)

    # Train model
    history = mc.train(X_train_scaled, Y_train,X_valid_scaled, Y_valid, model, epochs=100)

    # Predict model
    y_mean, y_std = mc.predict(model, X_test_scaled)
    print("y mean is", y_mean, "std is", y_std, y_mean.shape, y_std.shape)

    # Evaluate loss
    train_nll, train_mse, train_mae, train_acc, train_mape = model.evaluate(X_train_scaled, Y_train, verbose=2)
    valid_nll, val_mse, val_mae, val_acc, val_mape = model.evaluate(X_valid_scaled, Y_valid, verbose=2)
    test_nll, test_mse, test_mae, test_acc, test_mape = model.evaluate(X_test_scaled, Y_test, verbose=2)
    print("train loss", train_nll, train_mse, train_mae, train_acc, train_mape)
    print("valid loss", valid_nll, val_mse, val_mae, val_acc, val_mape)
    print("test loss", test_nll, test_mse, test_mae, test_acc, test_mape)

    # Calculate error metrics
    average_output = np.mean(df['LE_bowen_corr(mm)'])
    data_size = df.shape[0]
    metrics = dataPreparation.error_metrics(Y_test, y_mean, average_output, data_size, nb_columns=48, nb_params=1)
    metrics["NLL"] = test_nll
    print("metrics", metrics)

    def plot_regression_model_analysis(gt=None,  trn=None, tst=None,
                                   pred=None, xlim=None, ylim=None,
                                   title=None):
        if gt:
            x_gt, y_gt = gt
            plt.plot(x_gt, y_gt, c='#F0AA00', 
                    alpha=.8, lw=2, label="ground truth")
        if trn:
            x_trn, y_trn = trn
            plt.scatter(x_trn,  y_trn,
                        s=8, ec='black', lw=1, fc=None, alpha=1,
                        label='train samples')
        
        if tst:
            x_tst, y_tst = tst
            plt.scatter(x_tst, y_tst, s=5, c='blue', alpha=.1, label='test samples')

        if pred:
            x_tst, yhat_mean, yhat_std = pred
            plt.scatter(x_tst, yhat_mean, s=5, c='magenta', alpha=1, label='preds')
            if yhat_std is not None:
                plt.fill_between(x_tst, (yhat_mean - 1.*yhat_std), (yhat_mean + 1.*yhat_std), lw=1,
                                ec='blue', fc='blue', alpha=.3, label='preds 1*std')
                plt.fill_between(x_tst, (yhat_mean - 2.*yhat_std), (yhat_mean + 2.*yhat_std), lw=1,
                                ec='blue', fc='blue', alpha=.2, label='preds 2*std')

        if xlim:
            plt.xlim(*xlim)
            
        if ylim:
            plt.ylim(*ylim)
        
        if title:
            plt.title(title)
            
        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend(bbox_to_anchor=(1.35, 1.03), loc='upper right', fancybox=False, framealpha=1.0)

    index = 1000

    plot_regression_model_analysis(gt=None,
                                   trn=(X_train_scaled[:, 0], Y_train),
                                   pred=(X_test_scaled[:, 0], y_mean, y_std),
                                   title='MC Dropout Regression')
    plt.show()
    plt.close()

    def plot_errors_mc_dropout(Yt_hat, X, y, T):
        pred = np.zeros((X.shape[0], T))  # empty array to be populated
        means = []  # save mean for each predicted point
        std = []  # save standard dev for each predicted
        for j in range(X.shape[0]):
            for i in range(T):
                pred[j][i] = Yt_hat[i][j]
            means.append(pred[j].mean())  # get the mean for each prediction
            std.append(pred[j].std())  # get the standard deviation

        plt.figure(figsize=(16, 16))  # make the size of the plot a bit bigger
        plt.errorbar(x=list(range(X.shape[0])), y=means, yerr=std, fmt='x', label='errors')
        plt.scatter(list(range(X.shape[0])), y, c='r',
                    label='real')  # add the real values on top with red color
        plt.legend(loc='best')

        plt.show()
        plt.close()
    
    # plot_errors_mc_dropout(y_mean, X_test_scaled, Y_test, 20)
        
        
        

       