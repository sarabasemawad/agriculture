import numpy as np
import pandas as pd
import matplotlib.pyplot as plt



paths = ['ensemble_data', 'mcdropout_data', 'mdn_data']
types = ['Ensemble', 'MCDropout', 'MDN']
columns = ['Site Id', 'Date', 'Aleatoric Uncertainty',
       'Epistemic Uncertainty', 'Total Uncertainty']
num_rows = 100
selected_site_id = 'US-SO2'
#'FI-Jok'
#'US-Bi2'
df_list = {}
for i in range(len(paths)):
    path = paths[i]
    model_type = types[i]
    df = pd.read_csv(path + ".csv", delimiter=',')
    df = df[columns]
    # df['Date'] = pd.to_datetime(df.Date)
    # df.sort_values(by=['Date'], inplace=True, ascending=True)
    # df.reset_index(drop=True)
    df['Model'] = model_type
    # if selected_site_id != '':
    #     df = df[df['Site Id'] == selected_site_id]
    df = df[:num_rows]
    df.drop(['Site Id', 'Date', 'Aleatoric Uncertainty',
       'Epistemic Uncertainty'], axis=1, inplace=True)
    print('df size', df.shape[0])
    df_list[model_type] = df

print(df_list['Ensemble'].columns)

fig, ax = plt.subplots()
for df in df_list.keys():
    df_list[df].plot(ax=ax)
plt.legend(types, loc='upper left')
ax.set_xlabel('Data points')
ax.set_ylabel('Total Uncertainty')
plt.suptitle('Total Uncertainty for Site ' + selected_site_id)
plt.show()

