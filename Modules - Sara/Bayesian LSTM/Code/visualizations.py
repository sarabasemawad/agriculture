import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import plotly.graph_objects as go

from DataPreparation import DataPreparation 

dataPreparation = DataPreparation()
input_path = "/Users/saraawad/Desktop/Modules Code/Bayesian LSTM/Experiments/"
output_path = "/Users/saraawad/Desktop/Modules Code/Bayesian LSTM/Experiments/"
output_column = 'LE_bowen_corr(mm)'
#'le_mean'
#'LE_bowen_corr(mm)'

df_true = pd.read_csv(input_path + "true_plot_data.csv", delimiter=',')
df_test = pd.read_csv(input_path + "test_plot_data.csv", delimiter=',')

print("Truth:" , df_true.columns)
print("Test:" , df_test.columns)

def plot_uncertainty(df_true, df_test, output_column, date_range=[], isFull=False, isYear=True, isSample=False, num_samples=100):
    df_test_plt = df_test.copy(deep=True)
    df_true_plt = df_true.copy(deep=True)
    
    if len(date_range) > 0:
        if isFull:
            df_test_plt = df_test_plt.loc[df_test_plt['Date'].between(date_range[0], date_range[1])]
            df_true_plt = df_true_plt.loc[df_true_plt['Date'].between(date_range[0], date_range[1])]
        elif isYear and isSample == False:
            df_test_plt['Year'] = pd.DatetimeIndex(df_test_plt['Date']).year
            df_true_plt['Year'] = pd.DatetimeIndex(df_true_plt['Date']).year

            df_test_plt = df_test_plt.loc[df_test_plt['Year'].between(date_range[0], date_range[1])]
            df_true_plt = df_true_plt.loc[df_true_plt['Year'].between(date_range[0], date_range[1])]

        elif isYear and isSample:
            df_test_plt['Year'] = pd.DatetimeIndex(df_test_plt['Date']).year
            df_true_plt['Year'] = pd.DatetimeIndex(df_true_plt['Date']).year

            df_test_plt = df_test_plt.loc[df_test_plt['Year'].between(date_range[0], date_range[1])]
            df_true_plt = df_true_plt.loc[df_true_plt['Year'].between(date_range[0], date_range[1])]

            df_test_plt = df_test_plt[:num_samples]
            df_true_plt = df_true_plt[:num_samples]

    else:
        if isSample:
            df_test_plt = df_test_plt[:num_samples]
            df_true_plt = df_true_plt[:num_samples]

    upper_trace = go.Scatter(
        x=df_test_plt['Date'],
        y=df_test_plt['upper_bound'],
        mode='lines',
        fill=None,
        name='99% Upper Confidence Bound'
        )
    lower_trace = go.Scatter(
        x=df_test_plt['Date'],
        y=df_test_plt['lower_bound'],
        mode='lines',
        fill='tonexty',
        fillcolor='rgba(255, 211, 0, 0.5)',
        name='99% Lower Confidence Bound'
        )
    real_trace = go.Scatter(
        x=df_true_plt['Date'],
        y=df_true_plt[output_column],
        mode='lines',
        fill=None,
        name='Real Values'
        )

    data = [upper_trace, lower_trace, real_trace]

    fig = go.Figure(data=data)
    fig.update_layout(title='Uncertainty Quantification for LE Test Data',
                       xaxis_title='Time',
                       yaxis_title='LE')

    fig.show()
    return df_true_plt, df_test_plt
    

# Plot uncertainty for a certain date interval
# plot_uncertainty(df_true, df_test, output_column, date_range=['2016-05-01', '2016-05-07'], isFull=True, isYear=False)

# Plot uncertainty for a certain year interval
# plot_uncertainty(df_true, df_test, output_column, date_range=[2016, 2016], isFull=False, isYear=True)

# Plot uncertainty for all the data
# plot_uncertainty(df_true, df_test, output_column, date_range=[])

# Plot uncertainty for certain number of rows
plot_uncertainty(df_true, df_test, output_column, date_range=[], isSample=True, num_samples=50)

# Plot uncertainty for a certain year and number of samples
plot_uncertainty(df_true, df_test, output_column, date_range=[2016, 2017], isFull=False, isYear=True, isSample=True, num_samples=100)