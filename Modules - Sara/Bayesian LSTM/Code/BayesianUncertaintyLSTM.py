import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json
import os
import time

import plotly.express as px
import plotly.graph_objects as go

# Torch Libraries
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.metrics.cluster import normalized_mutual_info_score
from scipy.stats.stats import pearsonr, spearmanr
from scipy.spatial import distance
from collections import OrderedDict


#Scripts imports
from DataPreparation import DataPreparation 
import BayesianLSTM

## Global Variables

output_path = "/Users/saraawad/Desktop/Modules Code/Bayesian LSTM/Experiments/"
file_name = output_path + "test_scores.csv"
# Specify batch size
batch_size = 64
# Specify number of epochs
n_epochs = 150
# Split data into 70 20
train_split = 0.7
# Specify sequence length for the sliding window size
sequence_length = 10
# Specify learning rate
lr = 0.01

# Run test data for several number of experiments
n_experiments = 100

#TODO: save hyper parameters
#TODO: check why output is not saved


# Helpers
def plot_LE_time(df, output_column):   
    plot_length = 100
    plot_df = df.copy(deep=True).iloc[:plot_length]
    plot_df['weekday'] = plot_df['Date'].dt.weekday

    fig = px.line(plot_df,
                x="Date",
                y=output_column, 
                color="weekday", 
                title="Distribution of LE per Week Days")
    fig.show()

def plot_LE_train_test_time(df, output_column, date_range, title):
    df['year'] = df['Date'].dt.year.astype(int)
    fig = px.line(df.loc[df['year'].between(date_range[0], date_range[1])],
                 x="Date",
                 y=output_column,
                 color="source",
                 title="LE comparing train versus test for"  + title)
    fig.show()



def evaluate_train_test_scores(df, output_column, training_time, testing_time):
    offset = sequence_length
    training_df = pd.DataFrame()
    training_df['Date'] = df['Date'].iloc[offset:n_train + offset:1] 
    training_predictions = bayesian_lstm.predict(X_train)
    training_df[output_column] = inverse_transform(training_predictions)
    training_df['source'] = 'Training Prediction'

    training_truth_df = pd.DataFrame()
    training_truth_df['Date'] = training_df['Date']
    training_truth_df[output_column] = resample_df[output_column].iloc[offset:n_train + offset:1] 
    training_truth_df['source'] = 'True Values'

    testing_df = pd.DataFrame()
    testing_df['Date'] = df['Date'].iloc[n_train + offset::1] 
    testing_predictions = bayesian_lstm.predict(X_test)
    testing_df[output_column] = inverse_transform(testing_predictions)
    testing_df['source'] = 'Test Prediction'

    testing_truth_df = pd.DataFrame()
    testing_truth_df['Date'] = testing_df['Date']
    testing_truth_df[output_column] = df[output_column].iloc[n_train + offset::1] 
    testing_truth_df['source'] = 'True Values'

    train_metrics = error_metrics(training_truth_df[output_column], training_df[output_column], len(feature_columns))
    test_metrics = error_metrics(testing_truth_df[output_column], testing_df[output_column], len(feature_columns))
    print("Train Metrics:", train_metrics)
    print("Test Metrics:", test_metrics)

    train_metrics["Training Time (seconds)"] = training_time
    train_metrics["Testing Time (seconds)"] = testing_time

    test_metrics["Training Time (seconds)"] = training_time
    test_metrics["Testing Time (seconds)"] = testing_time

    export_scores(test_metrics, "Manual Testing LSTM")
    export_scores(train_metrics, "Manual Training LSTM")
    return training_df, testing_df, training_truth_df, testing_truth_df

# Get lower and upper bound with 3 standard deviations away from the mean
def compute_bounds(df, step, mean_name='le_mean', std_name='le_std'):
    df['lower_bound'] = df[mean_name] - step*df[std_name]
    df['upper_bound'] = df[mean_name] + step*df[std_name]
    df.to_csv(output_path + 'test_confidence.csv')
    return df

def plot_uncertainty(df_true, df_test, output_column, date_range=[]):
    df_test_plt = df_test.copy(deep=True)
    df_true_plt = df_true.copy(deep=True)

    df_true_plt.to_csv(output_path + 'true_plot_data.csv')
    df_test_plt.to_csv(output_path + 'test_plot_data.csv')
    
    if len(date_range) > 0:
        df_test_plt = df_test_plt.loc[df_test_plt['Date'].between(date_range[0], date_range[1])]
        df_true_plt = df_true_plt.loc[df_true_plt['Date'].between(date_range[0], date_range[1])]

    upper_trace = go.Scatter(
        x=df_test_plt['Date'],
        y=df_test_plt['upper_bound'],
        mode='lines',
        fill=None,
        name='99% Upper Confidence Bound'
        )
    lower_trace = go.Scatter(
        x=df_test_plt['Date'],
        y=df_test_plt['lower_bound'],
        mode='lines',
        fill='tonexty',
        fillcolor='rgba(255, 211, 0, 0.5)',
        name='99% Lower Confidence Bound'
        )
    real_trace = go.Scatter(
        x=df_true_plt['Date'],
        y=df_true_plt[output_column],
        mode='lines',
        fill=None,
        name='Real Values'
        )

    data = [upper_trace, lower_trace, real_trace]

    fig = go.Figure(data=data)
    fig.update_layout(title='Uncertainty Quantification for LE Test Data',
                       xaxis_title='Time',
                       yaxis_title='LE')

    fig.show()
    return df_true_plt, df_test_plt
    
    
def compute_confidence_interval(df_true_plt, df_test_plt, output_column):
    bounds_df = pd.DataFrame()

    # Using 99% confidence bounds
    bounds_df['lower_bound'] = df_test_plt['lower_bound']
    bounds_df['prediction'] = df_test_plt['le_mean']
    bounds_df['real_value'] = df_true_plt[output_column]
    bounds_df['upper_bound'] = df_test_plt['upper_bound']

    bounds_df['contained'] = ((bounds_df['real_value'] >= bounds_df['lower_bound']) &
                              (bounds_df['real_value'] <= bounds_df['upper_bound']))

    print("Proportion of points contained within 99% confidence interval:", 
          bounds_df['contained'].mean())
    return bounds_df['contained'].mean()

# Create sliding window
def create_sliding_window(data, sequence_length, stride=1):
    X_list, y_list = [], []
    for i in range(len(data)):
      if (i + sequence_length) < len(data):
        X_list.append(data.iloc[i:i+sequence_length:stride, :].values)
        y_list.append(data.iloc[i+sequence_length, -1])
    return np.array(X_list), np.array(y_list)

# Calculate error metrics
def error_metrics(y_test, y_pred, nb_columns):
    n = len(y_test)
    #convert to float for it to work for AIC and BIC data should all be of the same format i.e float not mix of types
    y_test_f =  [float(item) for item in y_test]
    y_predict_f =  [float(item) for item in y_pred]
    test_scores = {}
    r2_Score = r2_score(y_test, y_pred)  # r-squared
    adjusted_r2 = 1 - ((1 - r2_Score) * (n - 1)) / (n - nb_columns - 1)  # adjusted r-squared
    rmse_score = np.sqrt(mean_squared_error(y_test, y_pred))  # RMSE
    mse_score = mean_squared_error(y_test_f, y_predict_f)  # MSE
    mae_score = mean_absolute_error(y_test, y_pred)  # MAE
    mape_score = np.asarray(np.abs(( np.array(y_test) - np.array(y_pred)) / np.array(y_test)), dtype=np.float64).mean() * 100  # MAPE
    re = (mse_score / np.mean(y_pred)) * 100
    accuracy = 100 - np.mean(mape_score)
    spearman_corr, _ = spearmanr(y_test, y_pred)
    r2 = str(round(r2_Score, 2))
    adjusted_r2 = str(round(adjusted_r2, 2))
    mae = str(round(mae_score, 2))
    mse = str(round(mse_score, 2))
    rmse = str(round(rmse_score, 2))
    re = str(round(re, 2))
    spearman = str(round(spearman_corr, 2))
    mape = str(round(mape_score, 2))
    accuracy = str(round(accuracy, 2))
    nmi = normalized_mutual_info_score(np.array(y_test_f), np.array(y_predict_f))

    num_params = 1
    def calculate_aic(n, mse, num_params):
        aic = n * np.log(mse) + 2 * num_params
        return aic

    def calculate_bic(n, mse, num_params):
        bic = n * np.log(mse) + num_params * np.log(n)
        return bic

    aic = str(round(calculate_aic(len(y_test), mse_score, num_params), 2))
    bic = str(round(calculate_bic(len(y_test), mse_score, num_params), 2))

    pearson_corr, _ = pearsonr(np.array(y_test_f), np.array(y_predict_f))
    distance_corr = distance.correlation(np.array(y_test_f), np.array(y_predict_f))
    
    test_scores["Average"] = str(np.mean(resample_df[output_column]))
    test_scores["Average Testing"] = str(np.mean(y_test))
    test_scores["R2"] = r2
    test_scores["Adjusted R2"] = adjusted_r2
    test_scores["RMSE"] = rmse
    test_scores["MSE"] = mse
    test_scores["MAE"] = mae
    test_scores["MAPE"] = mape
    test_scores["Accuracy"] = accuracy
    test_scores["Pearson C.C."] = pearson_corr
    test_scores["Spearman C.C."] = spearman
    test_scores["Spatial Distance"] = distance_corr
    test_scores["NMI"] = nmi
    test_scores["Data Size"] = str(data_size)
    test_scores["AIC"] = aic
    test_scores["BIC"] = bic
    test_scores["Relative Error"] = re
    return test_scores

# Inverse the transform applied on output
def inverse_transform(y):
    return target_scaler.inverse_transform(y.reshape(-1, 1))


def write_dict_to_json(path, content):
    with open(path + '.json', 'w') as file:
        file.write(json.dumps(content, ensure_ascii=False))
    
def write_str_to_txt(path, content):
    text_file = open(path + ".txt", "w")
    text_file.write(content) 
    text_file.close()

def plot_actual_vs_predicted(actual_column, predicted_column):
    plt.plot(list(range(1, len(actual_column) + 1)), actual_column, color='b', label='actual')
    plt.plot(list(range(1, len(actual_column) + 1)), predicted_column, color='r', label='predicted')
    plt.legend(loc='best')
    plt.suptitle('actual vs. predicted')
    plt.savefig(output_path + 'actual_vs_predicted')
    plt.close()
    
def plot_actual_vs_predicted_scatter_bisector(actual_column, predicted_column):
    fig, ax = plt.subplots()
    ax.scatter(actual_column, predicted_column)
    ax.plot([actual_column.min(), actual_column.max()], [predicted_column.min(), predicted_column.max()], 'k--', lw=4)
    ax.set_xlabel('Actual')
    ax.set_ylabel('Predicted')
    plt.suptitle('actual vs. predicted')
    plt.savefig(output_path + 'actual_vs_predicted_scatter_plot')
    plt.close()

def export_scores(scores, columnName):
    if not os.path.exists(file_name):
        df = pd.DataFrame(list())
        df.to_csv(file_name, index=False)
    else:
        df = pd.read_csv(file_name, delimiter=',')
    df["Error Metrics"] = scores.keys()
    print("keys", scores.keys())
    df[columnName] = scores.values()
    df.to_csv(file_name, index=False)
    return df


class BayesianLSTM(nn.Module):
    def __init__(self, n_features, output_length):

        super(BayesianLSTM, self).__init__()

        self.hidden_size_1 = 128
        self.hidden_size_2 = 32
        self.n_layers = 1 # number of (stacked) LSTM layers

        self.lstm1 = nn.LSTM(n_features, 
                             self.hidden_size_1, 
                             num_layers=1,
                             batch_first=True)
        self.lstm2 = nn.LSTM(self.hidden_size_1,
                             self.hidden_size_2,
                             num_layers=1,
                             batch_first=True)
        
        self.dense = nn.Linear(self.hidden_size_2, output_length)
        self.loss_fn = nn.MSELoss()
        
    def forward(self, x):
        batch_size, seq_len, _ = x.size()

        hidden = self.init_hidden1(batch_size)
        output, _ = self.lstm1(x, hidden)
        output = F.dropout(output, p=0.5, training=True)
        state = self.init_hidden2(batch_size)
        output, state = self.lstm2(output, state)
        output = F.dropout(output, p=0.5, training=True)
        output = self.dense(state[0].squeeze(0))
        
        return output
        
    def init_hidden1(self, batch_size):
        hidden_state = Variable(torch.zeros(self.n_layers, batch_size, self.hidden_size_1))
        cell_state = Variable(torch.zeros(self.n_layers, batch_size, self.hidden_size_1))
        return hidden_state, cell_state
    
    def init_hidden2(self, batch_size):
        hidden_state = Variable(torch.zeros(self.n_layers, batch_size, self.hidden_size_2))
        cell_state = Variable(torch.zeros(self.n_layers, batch_size, self.hidden_size_2))
        return hidden_state, cell_state
    
    def loss(self, pred, truth):
        return self.loss_fn(pred, truth)

    def predict(self, X):
        return self(torch.tensor(X, dtype=torch.float32)).view(-1).detach().numpy()

# Load and process data set
dataPreparation = DataPreparation()
df = dataPreparation.read_data()
output_column = "LE_bowen_corr(mm)"

# Save date and site id for export
dates_list = df["Date"]
sites_list = df["Site Id"]

df = df.drop(["Site Id"], axis = 1)

selected_columns = []
selected_columns.extend(df.columns)
resample_df = df
# resample_df['log_le'] = resample_df[output_column]
target_column = output_column
feature_columns = selected_columns + [target_column]
feature_columns.remove(output_column)
print(feature_columns)

resample_df = resample_df[feature_columns]
# resample_df.rename(columns={'log_le': output_column}, inplace=True)
resample_df.head()
print("shape is", resample_df.shape, "final columns:", resample_df.columns)

n_train = int(train_split * len(resample_df))
n_test = len(resample_df) - n_train

features = list(feature_columns)
features.remove("Date")
feature_array = resample_df[features].values

# Fit Scaler only on Training features
feature_scaler = MinMaxScaler()
feature_scaler.fit(feature_array[:n_train])
# Fit Scaler only on Training target values
target_scaler = MinMaxScaler()
target_scaler.fit(feature_array[:n_train, -1].reshape(-1, 1))

# Transfom on both Training and Test data
scaled_array = pd.DataFrame(feature_scaler.transform(feature_array),
                            columns=features)

X, y = create_sliding_window(scaled_array, 
                             sequence_length)

# Save X test with date and site id
X_df = resample_df
X_df["Date"] = dates_list
X_df["Site Id"] = sites_list

X_train_df = X_df.iloc[:n_train, :]
X_test_df = X_df.iloc[n_train:, :]

X_train = X[:n_train]
y_train = y[:n_train]

X_test = X[n_train:]
y_test = y[n_train:]

print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)
data_size = X_train.shape[0] + X_test.shape[0]

# Begin Training
n_features = scaled_array.shape[-1]
print("number of features", n_features)
output_length = 1
bayesian_lstm = BayesianLSTM(n_features=n_features,
                             output_length=output_length)


# Specify loss function MSE is used
criterion = torch.nn.MSELoss()
# Specify optimizer, Adam is used
optimizer = torch.optim.Adam(bayesian_lstm.parameters(), lr=lr)

# Save start training time
start_time = time.time()
print("Training Start time:", time.time())

bayesian_lstm.train()
for e in range(1, n_epochs+1):
    for b in range(0,len(X_train),batch_size):
        features = X_train[b:b+batch_size,:,:]
        target = y_train[b:b+batch_size]    
        # Get batches of X and Y
        X_batch = torch.tensor(features,dtype=torch.float32)    
        y_batch = torch.tensor(target,dtype=torch.float32)

        output = bayesian_lstm(X_batch) 
        loss = criterion(output.view(-1), y_batch) 

        loss.backward()
        optimizer.step()        
        optimizer.zero_grad() 

    # Print the result each 10 epochs
    if e % 10 == 0:
      print('epoch: ', e, 'loss: ', loss.item())

# Get mean of output feature
print("average of target is", np.mean(inverse_transform(y_train)))
# Save end training time
training_time = (time.time() - start_time)
print("Training time in seconds: %.5f seconds" % training_time)

# Save start testing time
start_time = time.time()
print("Testing Start time:", start_time)

# Get predictions
testing_predictions = bayesian_lstm.predict(X_test)
# Save end testing time
testing_time = (time.time() - start_time)
print("Testing time in seconds: %.5f seconds" % testing_time)

# Calculate error metrics after inversing the scaling that is applied to the output
test_scores = error_metrics(inverse_transform(y_test), inverse_transform(testing_predictions), len(features))
# test_scores["Training Time (seconds)"] = str(training_time)
# test_scores["Testing Time (seconds)"] = str(esting_time)
print("metrics:", test_scores)
# export_scores(output_path + test_scores, "Manual Testing LSTM")

# Export data sets
y_test_unscaled = inverse_transform(y_test)
y_predict_unscaled = inverse_transform(testing_predictions)

y_test_df = pd.DataFrame(y_test_unscaled)
y_predict_df = pd.DataFrame(y_predict_unscaled)

y_test_full_df = pd.DataFrame(y_test_df)
y_test_full_df[output_column + "_predicted"] = y_predict_df
y_test_full_df.to_csv(output_path + 'test_output_dataset.csv')

X_test_df = X_test_df.dropna()
X_test_df[output_column + "_predicted"] = y_predict_df
X_test_df.head()
X_test_df.to_csv(output_path + 'test_dataset.csv')
X_train_df.to_csv(output_path + 'train_dataset.csv')

# Evaluate model performance
training_df, testing_df, training_truth_df, testing_truth_df = evaluate_train_test_scores(df, output_column, training_time, testing_time)
evaluation_df = pd.concat([training_df, 
                            testing_df,
                            training_truth_df,
                            testing_truth_df
                            ], axis=0)


# Plot training and test
plot_actual_vs_predicted(y_test_unscaled, y_predict_unscaled)
plot_actual_vs_predicted_scatter_bisector(y_test_unscaled, y_predict_unscaled)

# Plot train versus test for certain date range
plot_LE_train_test_time(evaluation_df, output_column, [2018, 2018], "2018")


# Uncertainty study
test_uncertainty_df = pd.DataFrame()
test_uncertainty_df['Date'] = testing_df['Date']

for i in range(n_experiments):
  experiment_predictions = bayesian_lstm.predict(X_test)
  test_uncertainty_df['le_{}'.format(i)] = inverse_transform(experiment_predictions)

# Calculate mean and standard deviation for LE according to test predictions
le_df = test_uncertainty_df.filter(like='le', axis=1)
test_uncertainty_df['le_mean'] = le_df.mean(axis=1)
test_uncertainty_df['le_std'] = le_df.std(axis=1)

test_uncertainty_df = test_uncertainty_df[['Date', 'le_mean', 'le_std']]


# Get bounds for a certain year
test_uncertainty_df = compute_bounds(test_uncertainty_df, 3)
print(test_uncertainty_df)

df_true_plt, df_test_plt = plot_uncertainty(testing_truth_df,
                                            test_uncertainty_df,
                                            output_column,
                                            date_range=['2016-05-01', '2016-05-09'])

print(compute_confidence_interval(df_true_plt, df_test_plt, output_column))

# Get bounds for all the data
test_uncertainty_df = compute_bounds(test_uncertainty_df, 3)
print(test_uncertainty_df)

df_true_plt, df_test_plt = plot_uncertainty(testing_truth_df,
                                            test_uncertainty_df,
                                            output_column,
                                            date_range=[])

print(compute_confidence_interval(df_true_plt, df_test_plt, output_column))
