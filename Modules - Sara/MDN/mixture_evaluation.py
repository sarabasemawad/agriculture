from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

import numpy as np
import json
import time
import warnings
warnings.filterwarnings("ignore")

import sys
# sys.path.insert(1, '/Users/saraawad/Desktop/Modules Code/Uncertainty/MCDO-master/common/')

from mixture_training import mixture_training
from dataset_preparation import DataPreparation
import plotting


def mixture_evaluation(X_train, Y_train, X_test, Y_test, X_test_exp, dropout, learning_rate, n_mixtures, epochs, num_rows, ax):
    # Save start training time
    start_time = time.time()
    print("Training Start time:", time.time())

    sess, x_placeholder, dropout_placeholder = mixture_training(X_train, Y_train, dropout, learning_rate, epochs, n_mixtures)
    
    # Save end training time
    training_time = (time.time() - start_time)
    print("Training time in seconds: %.5f seconds" % training_time)

    prediction_op = sess.graph.get_collection("prediction")
    uncertainty_op = sess.graph.get_collection("uncertainties")
    gmm_op = sess.graph.get_collection("gmm")


    # Save start testing time
    start_time = time.time()
    print("Testing Start time:", start_time)
    feed_dict = {x_placeholder: X_test,
                 dropout_placeholder: dropout}

    y_eval, uncertainties_eval = sess.run([prediction_op, uncertainty_op], feed_dict)
    # Save end testing time
    testing_time = (time.time() - start_time)
    print("Testing time in seconds: %.5f seconds" % testing_time)

    Y_predict = y_eval[0].flatten()

    aleatoric_eval, epistemic_eval = uncertainties_eval[0]
    total_uncertainty_eval = aleatoric_eval + epistemic_eval
    
     # Export test data set with predicted
    dataPreparation = DataPreparation()
    output_path = dataPreparation.output_path
    output_column = dataPreparation.output_column

    df_test = dataPreparation.export_test_dataset(X_test_exp, Y_predict, [aleatoric_eval, epistemic_eval, total_uncertainty_eval], ['Aleatoric Uncertainty', 'Epistemic Uncertainty', 'Total Uncertainty'], n_mixtures)


    # Remove un-used columns
    columns = []
    columns.extend(df.columns)
    columns.remove("Site Id")
    columns.remove("Date")
    columns.remove(dataPreparation.output_column)
    print("Columns", columns, len(columns))
    num_columns = len(columns)
    average_output = np.mean(df['LE_bowen_corr(mm)'])

    # Calculate error metrics
    Y_test.dropna(inplace=True)
    Y_test = np.array(Y_test)
    Y_predict = np.array(Y_predict).reshape(-1, 1)
    
    rel_points = np.array([[1, 0 , 0],[4, 0 , 0],[15, 1 , 0]])
    test_metrics = dataPreparation.evaluate(average_output, num_rows, num_columns, np.array(Y_test), np.array(Y_predict), 1, 0.1, "range", "high", 1.5, rel_points)
        
#     test_metrics = dataPreparation.error_metrics(np.array(Y_test), np.array(Y_predict), average_output, num_rows, num_columns, "")
    test_metrics["Training Time (seconds)"] = training_time
    test_metrics["Testing Time (seconds)"] = testing_time
    # Export metrics
    print("test metrics:", test_metrics)
    dataPreparation.export_scores(output_path + "error_mdn.csv", test_metrics, "Testing MDN NMixtures - " + str(n_mixtures))

    # Plot predictions with different uncertainties
    plotting.plot_mean_vs_truth_with_uncertainties(X_train, Y_train, X_test, Y_predict, aleatoric_eval, epistemic_eval, ax, 10)
    ax.legend()
    plt.close()

    # Plot training and test
    file_name = "mdn_" + str(n_mixtures)
    dataPreparation.plot_actual_vs_predicted(Y_test, Y_predict, file_name)
    dataPreparation.plot_actual_vs_predicted_scatter_bisector(Y_test, Y_predict, file_name)
    plt.close()

    plotting.plot_aleotoric(Y_test, Y_predict, Y_predict, aleatoric_eval, 10, output_path + "aleotoric_" + str(n_mixtures))
    plt.close()

    plotting.plot_epistemic(Y_test, Y_predict, aleatoric_eval, epistemic_eval, 10, output_path + "epistemic_" + str(n_mixtures))
    plt.close()

if __name__ == "__main__":
    # hyper params
    mixture_values = [1, 3, 5, 10, 15, 20]
    epochs = 3000
    dropout = 0.4
    learning_rate = 1e-3

    fig, axs = plt.subplots(len(mixture_values), 1, figsize=(30, 5*len(mixture_values)), sharey=True)
    fig.suptitle('Mixture-Model | Epochs: 20000, Learning Rate: 1e-3, Dropout 0.3', fontsize=20)

    # Load data set
    dataPreparation = DataPreparation()
    output_path = dataPreparation.output_path
    df = dataPreparation.read_data()
    num_rows = df.shape[0]

    # Save parameters
    parameters = {}
    parameters['number of epochs'] = epochs
    parameters['train split'] = 0.8
    parameters['dropout'] = dropout
    parameters['learning rate'] = learning_rate
    parameters['hidden 1'] = 128
    parameters['hidden 2'] = 128
    parameters['activation'] = 'Relu'
    parameters['number of layers'] = 2
    parameters['number of mixtures'] = mixture_values
    parameters['optimizer'] = 'Adam'
    parameters['distribution type'] = 'GeneralizedNormal'
    dataPreparation.write_dict_to_json(output_path + "parameters", parameters)


    X_train, Y_train, X_test, Y_test, X_test_exp = dataPreparation.process_train_test_data(df, scaleMinMax=True)
    print("X train", X_train.shape, "X test", X_test.shape)
    print("Y train", Y_train.shape, "Y test", Y_test.shape)

    for n_mixtures, ax in zip(mixture_values, axs):
        ax.set_title("%d Mixtures" % n_mixtures)
       
        mixture_evaluation(X_train, Y_train, X_test, Y_test, X_test_exp, dropout, learning_rate, n_mixtures, epochs, num_rows, ax)
        fig.savefig(output_path + "Mixture_Uncertainty.pdf")


